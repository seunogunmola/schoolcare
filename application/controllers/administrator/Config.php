<?php

class Config extends Admin_controller{

    function __construct() {
        parent::__construct();
    }


    function Schools($id = NULL, $delete = FALSE){
        if ($delete != 1){
            #IF ID SUPPLIED IT MEANS WE ARE UPDATING NEW USER
            if($id != NULL) {
                $this->data['school'] = $this->Schools_model->get($id);
                count($this->data['school']) || $this->data['errors'] = 'Data not found';
            }
            else {
                $this->data['school'] = $this->Schools_model->get_new();
            }
            #LOAD FORM VALIDATION RULES
            $rules = $this->Schools_model->rules;
            #PASS THE RULES TO FORM VALIDATION CLASS
            $this->form_validation->set_rules($rules) or die('I didnt set any rules');
            if ($this->form_validation->run() == TRUE) {
                    $fields = array('school_name','school_address','school_name','school_phone','school_email','school_state','school_country','status');
                    $data = $this->Schools_model->array_from_post($fields);
                    if ($id == NULL)
                        $data['uniqueid'] = $this->Schools_model->generate_unique_id($len = 10,$fieldname = "uniqueid");
                        $data['added_by'] = $this->data['current_user_session'];
                    #HASH THE SUPPLIED PASSWORD
                    $name_status = $this->Schools_model->_checkUniqueValue($value = $data['school_name'],$rowname = 'school_name',$tablename = 't_schools',$id);
                    if ($name_status == false)
                    {
                        $this->data['message'] = getAlertMessage("School name: <mark> ".$data['school_name']." </mark> already Exists, Please choose another one");
                    }
                    else {
                        $this->Schools_model->save($data,$id);
                        $this->session->set_flashdata('msg','School Saved Succesfully');
                        redirect('Administrator/Config/Schools');
                    }
            }
            else {
                 $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }
        }
        else{
            //CHECK IF ID IS SUPPLIED
            if (!empty($id)) {
             //GET DELETE DATA
                $delete_details = $this->Schools_model->get($id);
                if (count($delete_details)){
                    //DELETE RECURSIVELY
                    @unlink($delete_details->country_logo);
                    $deleted = $this->Schools_model->delete($id);
                    if ($deleted) {
                                    $this->session->set_flashdata('msg','School Deleted Successfully');
                                    redirect('Administrator/Config/Schools');
                    }
                    else {
                                    $this->session->set_flashdata('error','An Error Occured while deleting,please try again');
                                    redirect('Administrator/Config/Schools');
                    }
                }
                else {
                                    $this->session->set_flashdata('error','Data not Found');
                                    redirect('Administrator/Config/Schools');
                }

            }
        }
                    #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->db->select('t_schools.*,t_admin_users.fullname as created_by');
        $this->db->join('t_admin_users','t_admin_users.uniqueid = t_schools.added_by','inner');
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->db->get('t_schools')->result();
        $this->data['subview'] = 'Administrator/Config/Schools_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function Countries($id = NULL, $delete = FALSE){
        if ($delete != 1){
            #IF ID SUPPLIED IT MEANS WE ARE UPDATING
            if($id != NULL) {
                $this->data['country'] = $this->Countries_Model->get($id);
                count($this->data['country']) || $this->data['errors'] = 'Data not found';
            }
            else {
                $this->data['country'] = $this->Countries_Model->get_new();
            }
            #LOAD FORM VALIDATION RULES
            $rules = $this->Countries_Model->rules;

            #PASS THE RULES TO FORM VALIDATION CLASS
            $this->form_validation->set_rules($rules) or die('I didnt set any rules');


            if ($this->form_validation->run() == TRUE) {
                    $fields = array('country_name','country_abbreviation');
                    $data = $this->Schools_model->array_from_post($fields);
                    if ($id == NULL)
                        $data['uniqueid'] = $this->Countries_Model->generate_unique_id($len = 10,$fieldname = "uniqueid");
                    #HASH THE SUPPLIED PASSWORD
                    $country_name_status = $this->Countries_Model->_checkUniqueValue($value = $data['country_name'],$rowname = 'country_name',$tablename = 't_countries',$id);
                    if ($country_name_status == false)
                    {
                        $this->data['message'] = getAlertMessage("Country name: <mark> ".$data['country_name']." </mark> already Exists, Please choose another one");
                    }
                    else {

                        //HANDLE LOGO UPLOAD
                            if (isset($_FILES['country_logo']) && $_FILES['country_logo']['size'] > 0) {
                                $upload_path ='./resources/uploads/images/country_logos/';
                                if (!is_dir($upload_path))
                                    mkdir ($upload_path, 0777);
                                $source_file_name = 'country_logo';
                                $destination_file_name = mt_rand($min = 0000000000, $max = 999999999);
                                @$name = $_FILES[$source_file_name]["name"];
                                @$ext = end((explode(".", $name)));

                                if ($this->perform_upload($upload_path,$source_file_name,$destination_file_name))
                                {
                                    $data['country_logo'] = $upload_path.$destination_file_name.".".$ext;;
                                }
                                else {
                                    $this->session->set_flashdata('error',$this->data['upload_error']['error']);
                                    redirect('Administrator/Config/Countries');
                                }
                            }
                        $this->Countries_Model->save($data,$id);
                        $this->session->set_flashdata('msg','Country Saved Succesfully');
                        redirect('Administrator/Config/Countries');
                    }
            }
            else {
                 $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }
        }
        else{
            //CHECK IF ID IS SUPPLIED
            if (!empty($id)) {
             //GET DELETE DATA
                $delete_details = $this->Countries_Model->get($id);
                if (count($delete_details)){
                    //DELETE RECURSIVELY
                    @unlink($delete_details->country_logo);
                    $deleted = $this->Countries_Model->delete($id);
                    if ($deleted) {
                                    $this->session->set_flashdata('msg','Country Deleted Successfully');
                                    redirect('Administrator/Config/Countries');
                    }
                    else {
                                    $this->session->set_flashdata('error','An Error Occured while deleting,please try again');
                                    redirect('Administrator/Config/Countries');
                    }
                }
                else {
                                    $this->session->set_flashdata('error','Data not Found');
                                    redirect('Administrator/Config/Countries');
                }

            }
        }
                    #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['locations'] = $this->Schools_model->get();
        $this->data['countries'] = $this->Countries_Model->get();
        $this->data['subview'] = 'Administrator/Config/Countries_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }

    function Settings($id = NULL, $delete = FALSE){
            $settings_data = $this->Settings_Model->get();
            if(count($settings_data)) {
                $this->data['settings'] = $settings_data[0];
                $id = $this->data['settings']->id;
                //count($this->data['settings']) || $this->data['errors'] = 'Data not found';
            }
            else {
                $this->data['settings'] = $this->Settings_Model->get_new();
            }

            #LOAD FORM VALIDATION RULES
            $rules = $this->Settings_Model->rules;

            #PASS THE RULES TO FORM VALIDATION CLASS
            $this->form_validation->set_rules($rules) or die('I didnt set any rules');


            if ($this->form_validation->run() == TRUE) {
                    $fields = array('default_currency','vat','email_receivers','sms_receivers');
                    $data = $this->Settings_Model->array_from_post($fields);

                        $this->Settings_Model->save($data,$id);
                        $this->session->set_flashdata('msg','Settings Saved Succesfully');
                        redirect('Administrator/Config/Settings');
                    }

            else {
                 $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }

                    #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->data['subview'] = 'Administrator/Config/Settings_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }

    private function _handlePrivileges(){
                $privilege = $this->input->post('superadmin');
                if (strtoupper($privilege) == 'ON') {
                    $privilege = 'superadmin';
                }
                else {
                    $other_privileges = "";
                    foreach ( $this->input->post('privileges') as $value):
                        $other_privileges.=$value.'-';
                    endforeach;
                    $privilege = $other_privileges;
                }

                return rtrim($privilege,'-');
    }

}