<?php

class Outgoing_transactions extends Admin_controller{

    function __construct() {
        parent::__construct();
    }

    function index(){
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Outgoing_Transactions_Model->array_from_post($fields);

                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number'])){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Administrator/Outgoing_Transactions');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
               redirect('Administrator/Outgoing_Transactions/debit_wallet/'.$uniqueid);
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['students'] = $this->Students_model->get();
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->Schools_model->get();
        $this->data['subview'] = 'Administrator/Transactions/outgoing/account_select_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function debit_wallet($uniqueid){
        if(!isset($uniqueid)) {
            redirect('Administrator/Outgoing_Transactions');
        }
        //FETCH STUDENT RECORD
        $this->db->where('uniqueid',$uniqueid);
        $this->data['student_data'] = $this->db->get('t_students')->row();
        $this->data['account_balance'] = $this->_getAccountBalance($uniqueid);
        if (!count($this->data['student_data'])){
            $this->session->set_flashdata('error',"Student Record not Found - \n Possible Cause; Invalid Account Number Supplied \n Please try again");
            redirect('Administrator/Outgoing_Transactions');
        }
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'transaction_amount'=>array('field'=>'transaction_amount','label'=>'Transaction Amount','rules'=>'trim|required'),
                        'transaction_date'=>array('field'=>'transaction_date','label'=>'Transaction Date','rules'=>'trim|required'),
                        'transaction_purpose'=>array('field'=>'transaction_purpose','label'=>'Transaction Purpose','rules'=>'trim|required'),
                        'transaction_recepient'=>array('field'=>'transaction_recepient','label'=>'Transaction Recepient','rules'=>'trim|required'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('transaction_amount','transaction_date','transaction_purpose','transaction_recepient');
                $data = $this->Outgoing_Transactions_Model->array_from_post($fields);
                if($data[transaction_amount] > $this->data['account_balance']){
                    $this->session->set_flashdata('error',"Debit Amount is greater than current account number");
                    redirect('Administrator/Outgoing_Transactions/debit_wallet/'.$uniqueid);
                }
                else if ($data['transaction_amount'] <= 0) {
                    $this->session->set_flashdata('error','Transaction Amount Cannot be Zero or Less');
                    redirect('Administrator/Outgoing_Transactions/debit_wallet/'.$uniqueid);
                }

                $data['uniqueid'] = $this->Outgoing_Transactions_Model->generate_unique_id($len = 12,$fieldname = "uniqueid");
                $data['transaction_logged_by'] = $this->data['current_user_session'];
                $data['studentid'] = $uniqueid;
                $data['transaction_logged_date'] = date('Y-m-d h:i:s');

                $transaction_saved = $this->Outgoing_Transactions_Model->save($data,$id);
                if($transaction_saved){
                    //SEND SMS
                    $sms_string = "A Debit transaction occured on your School Care Account"
                            . "\n Beneficiary : ".$this->data['student_data']->surname.' '.$this->data['student_data']->othernames
                            . "\n Amount : NGN ".$data['transaction_amount']
                            . "\n Purpose : ".$data['transaction_purpose']
                            . "\n Date ".$data['transaction_date']
                            . " ";
                    
                    sendsms($sms_string,$this->data['student_data']->parent_phone_number);
                    $this->session->set_flashdata('success','Debit Transaction Saved Successfully');
                    redirect('Administrator/Outgoing_Transactions');
                }
                else{
                    $this->session->set_flashdata('error','An Error Occured while saving the transaction');
                    redirect('Administrator/Outgoing_Transactions');
                }
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['subview'] = 'Administrator/Transactions/Outgoing/debit_wallet_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function edit_debit_wallet($transaction_id){
        if(!isset($transaction_id)) {
            redirect('Administrator/Outgoing_Transactions');
        }
        //FETCH TRANSCTION RECORD
        $this->db->where('uniqueid',$transaction_id);
        $this->data['transaction_data'] = $transaction_data = $this->db->get('t_outgoing_transactions')->row();
        if (!count($this->data['transaction_data'])){
            $this->session->set_flashdata('error',"Transaction Record Not Found!");
            redirect('Administrator/Outgoing_Transactions/history');
        }

        //FETCH STUDENT RECORD
        $this->db->where('uniqueid',$transaction_data->studentid);
        $this->data['student_data'] = $this->db->get('t_students')->row();
        if (!count($this->data['student_data'])){
            $this->session->set_flashdata('error',"Student Record not Found - \n Possible Cause; Invalid Account Number Supplied \n Please try again");
            redirect('Administrator/Outgoing_Transactions/history');
        }
        $this->data['account_balance'] = $this->_getAccountBalance($transaction_data->studentid);
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'transaction_amount'=>array('field'=>'transaction_amount','label'=>'Transaction Amount','rules'=>'trim|required'),
                        'transaction_date'=>array('field'=>'transaction_date','label'=>'Transaction Date','rules'=>'trim|required'),
                        'transaction_purpose'=>array('field'=>'transaction_purpose','label'=>'Transaction Purpose','rules'=>'trim|required'),
                        'transaction_recepient'=>array('field'=>'transaction_recepient','label'=>'Transaction Recepient','rules'=>'trim|required'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('transaction_amount','transaction_date','transaction_purpose','transaction_recepient');
                $data = $this->Outgoing_Transactions_Model->array_from_post($fields);
                if($data[transaction_amount] > $this->data['account_balance']){
                    $this->session->set_flashdata('error',"Debit Amount is greater than current account number");
                    redirect('Administrator/Outgoing_Transactions/edit_debit_wallet/'.$uniqueid);
                }
                else if ($data['transaction_amount'] <= 0) {
                    $this->session->set_flashdata('error','Transaction Amount Cannot be Zero or Less');
                    redirect('Administrator/Outgoing_Transactions/edit_debit_wallet/'.$uniqueid);
                }
                $transaction_saved = $this->Outgoing_Transactions_Model->save($data,$transaction_data->id);
                if($transaction_saved){
                    $this->session->set_flashdata('msg','Transaction Editted Successfully');
                    redirect('Administrator/Outgoing_Transactions/History');
                }
                else{
                    $this->session->set_flashdata('error','An Error Occured while saving the transaction');
                    redirect('Administrator/Outgoing_Transactions');
                }
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['subview'] = 'Administrator/Transactions/Outgoing/edit_debit_wallet_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function delete($transaction_id) {
        if (isset($transaction_id)) {
            $this->db->where('uniqueid',$transaction_id);
            $transaction_deleted = $this->db->delete('t_outgoing_transactions');
            if($transaction_deleted){
                $this->session->set_flashdata('msg','Transaction Deleted Successfully');
            }
            else {
                $this->session->set_flashdata('error','An Error Occurred during delete operation \n You should try again');
            }

            redirect('Administrator/Outgoing_Transactions/history');
        }
    }
    function History(){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";
        $this->db->select('t_outgoing_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
        $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
        $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
        $this->db->order_by('t_outgoing_transactions.transaction_date DESC');
        $this->data['transactions'] = $this->Outgoing_Transactions_Model->get();
        $this->data['students'] = $this->Students_model->get();
        $this->data['subview'] = 'Administrator/Transactions/Outgoing/outgoing_transactions_history_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function Individual_student_transaction_history($studentid = NULL){
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($studentid != NULL || $this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Outgoing_Transactions_Model->array_from_post($fields);
                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number']) && $studentid == NULL){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Administrator/Outgoing_Transactions/History');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
                elseif(!empty($studentid)) {
                    $uniqueid = $studentid;
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
                $this->db->select('t_outgoing_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
                $this->db->where('t_outgoing_transactions.studentid',$uniqueid);
                $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
                $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
                $this->db->order_by('t_outgoing_transactions.transaction_date DESC');
                $this->data['transactions'] = $this->Outgoing_Transactions_Model->get();

                if(!count($this->data['transactions'])){
                    $this->session->set_flashdata('error',"No Transactions found for selected student");
                    redirect('Administrator/Outgoing_Transactions/History');
                }

        }
        else {
                redirect('Administrator/Outgoing_Transactions/History');
        }
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";

        $this->data['students'] = $this->Students_model->get();
        $this->data['subview'] = 'Administrator/Transactions/Outgoing/individual_student_transaction_history_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function print_receipt($transactionid){
        if(isset($transactionid)) {
            $this->db->select('t_outgoing_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
            $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
            $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
            $this->db->where('t_outgoing_transactions.uniqueid',$transactionid);
            $this->data['transaction_data'] = $this->db->get('t_outgoing_transactions')->row();
        }
        $this->data['subview'] = 'Administrator/Transactions/Outgoing/transaction_receipt_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }

}