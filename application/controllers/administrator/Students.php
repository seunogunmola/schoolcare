<?php

class Students extends Admin_controller{

    function __construct() {
        parent::__construct();
    }

    function index($id=null){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";

        #IF ID SUPPLIED IT MEANS WE ARE UPDATING NEW USER
        if($id != NULL) {
            $this->data['student'] = $this->Students_model->get($id);
            count($this->data['student']) || $this->data['errors'] = 'User not found';
        }
        else {
            $this->data['student'] = $this->Students_model->get_new();
        }

        #LOAD FORM VALIDATION RULES
        $rules = $this->Students_model->rules;
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');

        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('surname','othernames','gender','date_of_birth','school_id','parent_name','parent_email_address','parent_phone_number','status');
                $data = $this->Students_model->array_from_post($fields);
                if($id == NULL){
                    $data['uniqueid'] = $this->Students_model->generate_unique_id($len = 10,$fieldname = "uniqueid");
                    $data['passcode'] = $this->Students_model->hash('1234');
                }
                if (isset($_FILES['passport']) && $_FILES['passport']['size'] > 0) {
                    $data['passport'] = $this->_handlePassportUpload();
                }
                else if (!isset($_FILES['passport']) && $id ==  NULL){
                    $data['passport'] = './resources/uploads/images/students_passports/no_image.jpg';
                }
                $saved = $this->Students_model->save($data,$id) or die("I did not save data");
                if ($saved){
                    if($id == NULL){
                        $sms_string = "Your School Care has been created \n"
                                . "\n Account Name : ".$data['surname'].' '.$data['othernames']
                                . "\n Account No : ".$data['uniqueid']
                                . "\n Passcode : 1234"
                                . "Visit schoolcare.com for more info";
                        sendsms($sms_string,$data['parent_phone_number']);
                    }
                    $message = $id == NULL? 'User Added Succesfully':'User Updated Succesfully';
                    $this->session->set_flashdata('msg',$message);
                }
                else {
                    $this->session->set_flashdata('error','An Error Occured while creating account');
                }

                redirect('Administrator/Students/view');

        }
        else {
            //echo "I did not validate your rules";
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
           //redirect('Administrator/Students/index');
        }
        $this->data['students'] = $this->Students_model->get();
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->Schools_model->get();
        $this->data['subview'] = 'Administrator/Students/create_student_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }

    function view($id=null){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";

        $this->db->select('t_students.*,t_schools.school_name');
        $this->db->order_by('t_students.surname ASC, t_schools.school_name ASC');
        $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
        $this->data['students'] = $this->db->get('t_students')->result();
        $this->data['subview'] = 'Administrator/Students/view_students_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }

    function view_student($id){
        if(isset($id)) {
            $this->db->select('t_students.*,t_schools.school_name');
            $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
            $this->db->where('t_students.id',$id);
            $this->data['student'] = $this->db->get('t_students')->row();
        }
        $this->data['subview'] = 'Administrator/Students/view_single_student_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function print_access_card($id,$uniqueid){
        if(isset($id)) {
            $this->db->select('t_students.*,t_schools.school_name');
            $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
            $this->db->where('t_students.id',$id);
            $this->data['student'] = $this->db->get('t_students')->row();
        }
        $this->data['subview'] = 'Administrator/Students/print_student_access_card_page';
        $this->load->view('Administrator/_layout_main',$this->data);
    }
    function delete($id) {
        if (isset($id)) {
            $this->Students_model->delete($id);
            $this->session->set_flashdata('msg','User Deleted Successfully');
            redirect('Administrator/Students/view');
        }
    }

    private function _checkUserName($field,$id = NULL) {
        $this->db->where('username',$field);
        if ($id != NULL)
             $this->db->where('id !=',$id);
        $userNameData = $this->db->get('t_admin_users');
        if ($userNameData->num_rows() > 0) {

            return TRUE;
        }
        else return FALSE;
    }
    private function _checkEmailAddress($field,$id = NULL) {
        $this->db->where('email_address',$field);
        if ($id != NULL)
             $this->db->where('id !=',$id);
        $userNameData = $this->db->get('t_admin_users');
        if ($userNameData->num_rows() > 0) {

            return TRUE;
        }
        else return FALSE;
    }

    private function _handlePassportUpload(){
        if (isset($_FILES['passport']) && $_FILES['passport']['size'] > 0) {
            $upload_location = '';
            $upload_path ='./resources/uploads/images/students_passports/';
            if (!is_dir($upload_path))
                mkdir ($upload_path, 0777);
                $source_file_name = 'passport';
                $destination_file_name = mt_rand($min = 0000000000, $max = 999999999);
                @$name = $_FILES[$source_file_name]["name"];
                @$ext = end((explode(".", $name)));
                if ($this->perform_upload($upload_path,$source_file_name,$destination_file_name))
                    {
                        $upload_location = $upload_path.$destination_file_name.".".$ext;
                    }
                else {
                    $this->session->set_flashdata('error',$this->data['upload_error']['error']);
                    redirect('Administrator/Students');
                    }
            }
            return $upload_location;
    }

}