<?php

class Incoming_transactions extends Admin_controller{

    function __construct() {
        parent::__construct();
    }
    function index(){
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Incoming_Transactions_Model->array_from_post($fields);

                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number'])){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Agent/Incoming_transactions');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
               redirect('Agent/Incoming_transactions/fund_wallet/'.$uniqueid);
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['students'] = $this->Students_model->get();
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->Schools_model->get();
        $this->data['subview'] = 'Agent/Transactions/account_select_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function fund_wallet($uniqueid){
        if(!isset($uniqueid)) {
            redirect('Agent/Incoming_transactions');
        }
        //FETCH STUDENT RECORD
        $this->db->where('uniqueid',$uniqueid);
        $this->data['student_data'] = $this->db->get('t_students')->row();
        if (!count($this->data['student_data'])){
            $this->session->set_flashdata('error',"Student Record not Found - \n Possible Cause; Invalid Account Number Supplied \n Please try again");
            redirect('Agent/Incoming_transactions');
        }
        #LOAD FORM VALIDATION RULES
            $rules = array(
                        'transaction_amount'=>array('field'=>'transaction_amount','label'=>'Transaction Amount','rules'=>'trim|required'),
                        'transaction_date'=>array('field'=>'transaction_date','label'=>'Transaction Date','rules'=>'trim|required'),
                        'transaction_made_by'=>array('field'=>'transaction_made_by','label'=>'Transaction Made By','rules'=>'trim|required'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('transaction_amount','transaction_date','transaction_made_by');
                $data = $this->Incoming_Transactions_Model->array_from_post($fields);
                $data['uniqueid'] = $this->Incoming_Transactions_Model->generate_unique_id($len = 12,$fieldname = "uniqueid");
                $data['transaction_logged_by'] = $this->data['current_user_session'];
                $data['studentid'] = $uniqueid;
                $data['transaction_logged_date'] = date('Y-m-d h:i:s');
                if ($data['transaction_amount'] <= 0) {
                    $this->session->set_flashdata('error','Transaction Amount Cannot be Zero or Less');
                    redirect('Agent/Incoming_transactions/fund_wallet/'.$uniqueid);
                }
                $transaction_saved = $this->Incoming_Transactions_Model->save($data,$id = NULL);
                if($transaction_saved){
                    //SEND SMS
                    $sms_string = "A Credit transaction occured on your School Care Account"
                            . "\n Beneficiary : ".$this->data['student_data']->surname.' '.$this->data['student_data']->othernames
                            . "\n Amount : NGN ".$data['transaction_amount']
                            . "\n Date ".$data['transaction_date']
                            . " ";
                   // sendsms($sms_string,$this->data['student_data']->parent_phone_number);
                    $this->session->set_flashdata('success','Transaction Saved Successfully');
                    redirect('Agent/Incoming_transactions');
                }
                else{
                    $this->session->set_flashdata('error','An Error Occured while saving the transaction');
                    redirect('Agent/Incoming_transactions');
                }
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['subview'] = 'Agent/Transactions/fund_wallet_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function edit_fund_wallet($transaction_id){
        if(!isset($transaction_id)) {
            redirect('Agent/Incoming_transactions');
        }
        //FETCH TRANSCTION RECORD
        $this->db->where('uniqueid',$transaction_id);
        $this->data['transaction_data'] = $transaction_data = $this->db->get('t_incoming_transactions')->row();
        if (!count($this->data['transaction_data'])){
            $this->session->set_flashdata('error',"Transaction Record Not Found!");
            redirect('Agent/Incoming_transactions/history');
        }

        //FETCH STUDENT RECORD
        $this->db->where('uniqueid',$transaction_data->studentid);
        $this->data['student_data'] = $this->db->get('t_students')->row();
        if (!count($this->data['student_data'])){
            $this->session->set_flashdata('error',"Student Record not Found - \n Possible Cause; Invalid Account Number Supplied \n Please try again");
            redirect('Agent/Incoming_transactions/history');
        }
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'transaction_amount'=>array('field'=>'transaction_amount','label'=>'Transaction Amount','rules'=>'trim|required'),
                        'transaction_date'=>array('field'=>'transaction_date','label'=>'Transaction Date','rules'=>'trim|required'),
                        'transaction_made_by'=>array('field'=>'transaction_made_by','label'=>'Transaction Made By','rules'=>'trim|required'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('transaction_amount','transaction_date','transaction_made_by');
                $data = $this->Incoming_Transactions_Model->array_from_post($fields);
                if ($data['transaction_amount'] <= 0) {
                    $this->session->set_flashdata('error','Transaction Amount Cannot be Zero or Less');
                    redirect('Agent/Incoming_transactions/edit_fund_wallet/'.$transaction_id);
                }
                $transaction_saved = $this->Incoming_Transactions_Model->save($data,$transaction_data->id);
                if($transaction_saved){
                    $this->session->set_flashdata('msg','Transaction Editted Successfully');
                    redirect('Agent/Incoming_transactions/History');
                }
                else{
                    $this->session->set_flashdata('error','An Error Occured while saving the transaction');
                    redirect('Agent/Incoming_transactions');
                }
        }
        else {
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
        }
        $this->data['subview'] = 'Agent/Transactions/edit_fund_wallet_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function delete($transaction_id) {
        if (isset($transaction_id)) {
            $this->db->where('uniqueid',$transaction_id);
            $transaction_deleted = $this->db->delete('t_incoming_transactions');
            if($transaction_deleted){
                $this->session->set_flashdata('msg','Transaction Deleted Successfully');
            }
            else {
                $this->session->set_flashdata('error','An Error Occurred during delete operation \n You should try again');
            }

            redirect('Agent/Incoming_transactions/history');
        }
    }
    function History(){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";
        $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
        $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
        $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
        $this->db->order_by('t_incoming_transactions.transaction_date DESC');
        $this->data['transactions'] = $this->Incoming_Transactions_Model->get();
        $this->data['students'] = $this->Students_model->get();
        $this->data['subview'] = 'Agent/Transactions/incoming_transactions_history_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function Individual_student_transaction_history($studentid = NULL){
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($studentid != NULL || $this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Incoming_Transactions_Model->array_from_post($fields);
                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number']) && $studentid == NULL){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Agent/Incoming_transactions/History');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
                elseif(!empty($studentid)) {
                    $uniqueid = $studentid;
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
                $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
                $this->db->where('t_incoming_transactions.studentid',$uniqueid);
                $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
                $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
                $this->db->order_by('t_incoming_transactions.transaction_date DESC');
                $this->data['transactions'] = $this->Incoming_Transactions_Model->get();

                if(!count($this->data['transactions'])){
                    $this->session->set_flashdata('error',"No Transactions found for selected student");
                    redirect('Agent/Incoming_transactions/History');
                }

        }
        else {
                redirect('Agent/Incoming_transactions/History');
        }
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";

        $this->data['students'] = $this->Students_model->get();
        $this->data['subview'] = 'Agent/Transactions/individual_student_transaction_history_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function print_receipt($transactionid){
        if(isset($transactionid)) {
            $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
            $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
            $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
            $this->db->where('t_incoming_transactions.uniqueid',$transactionid);
            $this->data['transaction_data'] = $this->db->get('t_incoming_transactions')->row();
        }
        $this->data['subview'] = 'Agent/Transactions/transaction_receipt_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
}