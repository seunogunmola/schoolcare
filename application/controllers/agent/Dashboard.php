<?php

class Dashboard extends Agent_controller {

    function __construct() {
        parent::__construct();
    }
    function index() {
        $this->data['school_count'] = $this->getSchoolsCount();
        $this->data['student_count'] = $this->getStudentCount();
        $this->data['total_credit_transactions'] = $this->getCreditTransactionsCount();
        $this->data['total_debit_transactions'] = $this->getDebitTransactionsCount();
        $this->data['incoming_transactions_data'] = $this->getIncomingTransactionsDataForGraph();
        $this->data['outgoing_transactions_data'] = $this->getOutgoingTransactionsDataForGraph();
        $this->data['recent_credit_transactions'] = $this->getRecentCreditTransactions();
        $this->data['recent_debit_transactions'] = $this->getRecentDebitTransactions();
        $this->data['active_agents_count'] = $this->getActiveAgentsCount();
        $this->data['admin_user_count'] = $this->getAdminUserCount();
        $this->data['total_debit_transactions'] = $this->getTotalDebitTransactions();
        $this->data['total_credit_transactions'] = $this->getTotalCreditTransactions();
      //  $this->data['active_admin_users_count'] = $this->getRecentDebitTransactions();
        $this->data['students'] = $this->_getStudents();
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/morris-chart/morris.min.js") . '" ></script>';
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/morris-chart/raphael.min.js") . '" ></script>';
        $this->data['page_level_styles'] .= '<link rel = "stylesheet" type = "text/css" href ="' . getResource("css/morris-chart/morris.css") . '" ></script>';
        $this->data['subview'] = 'Agent/dashboard_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function quick_search(){
        #LOAD FORM VALIDATION RULES
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Students_model->array_from_post($fields);
                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number'])){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Agent/Dashboard');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
                //FETCH STUDENT RECORD
                $this->db->where('uniqueid',$uniqueid);
                $this->data['student_data'] = $this->db->get('t_students')->row();
                $this->data['account_balance'] = $this->_getAccountBalance($uniqueid);
                if (!count($this->data['student_data'])){
                    $this->session->set_flashdata('error',"Student Record not Found - \n Possible Cause; Invalid Account Number Supplied \n Please try again");
                    redirect('Agent/Dashboard');
                }

        }
        else {
             redirect('Agent/Dashboard');
        }
        $this->data['subview'] = 'Agent/quick_search_result_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }

    function getSchoolsCount(){
        $count = 0;
        $this->db->select('count(*) as schoolcount');
        $this->db->where('uniqueid',$this->data['agent_data']['schoolid']);
        $data = $this->db->get('t_schools')->row();
        if (count($data)){
                $count = $data->schoolcount;
        }
        return $count;
    }
    function getStudentCount(){
        $count = 0;
        $this->db->select('count(*) as studentcount');
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $data = $this->db->get('t_students')->row();
        if (count($data)){
                $count = $data->studentcount;
        }
        return $count;
    }
    function getCreditTransactionsCount(){
        $count = 0;
        $this->db->select('count(*) as total_credit_transactions');
        $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
        $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $data = $this->db->get('t_incoming_transactions')->row();
        if (count($data)){
                $count = $data->total_credit_transactions;
        }
        return $count;
    }
    function getDebitTransactionsCount(){
        $count = 0;
        $this->db->select('count(*) as total_debit_transactions');
        $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
        $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $data = $this->db->get('t_outgoing_transactions')->row();
        if (count($data)){
                $count = $data->total_debit_transactions;
        }
        return $count;
    }

    function getIncomingTransactionsDataForGraph(){
        $data_string = '';
        $this->db->select('transaction_date,sum(transaction_amount) as total_transactions');
        $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
        $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $this->db->group_by('transaction_date');
        $this->db->order_by('transaction_date DESC');
        $incoming_transactions_data = $this->db->get('t_incoming_transactions')->result();
        if(count($incoming_transactions_data)){
            foreach($incoming_transactions_data as $value){
              $data_string.="{ date :'".$value->transaction_date."', value : ".$value->total_transactions."},";
            }
            $data_string = rtrim($data_string,',');
        }
        return $data_string;
    }

    function getOutgoingTransactionsDataForGraph(){
        $data_string = '';
        $this->db->select('transaction_date,sum(transaction_amount) as total_transactions');
        $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
        $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
        $this->db->where('t_schools.uniqueid',$this->data['agent_data']['schoolid']);
        $this->db->group_by('transaction_date');
        $this->db->order_by('transaction_date DESC');
        $incoming_transactions_data = $this->db->get('t_outgoing_transactions')->result();
        if(count($incoming_transactions_data)){
            foreach($incoming_transactions_data as $value){
              $data_string.="{ date :'".$value->transaction_date."', value : ".$value->total_transactions."},";
            }
            $data_string = rtrim($data_string,',');
        }
        return $data_string;
    }

    function getRecentCreditTransactions(){
        $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames');
        $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $this->db->limit('5');
        $this->db->order_by('transaction_date DESC');
        $incoming_transactions_data = $this->db->get('t_incoming_transactions')->result();
        if(count($incoming_transactions_data)){
            return $incoming_transactions_data;
        }
        else {
            return false;
        }

    }
    function getRecentDebitTransactions(){
        $this->db->select('t_outgoing_transactions.*,t_students.surname,t_students.othernames');
        $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
        $this->db->limit('5');
        $this->db->order_by('transaction_date DESC');
        $outgoing_transactions_data = $this->db->get('t_outgoing_transactions')->result();
        if(count($outgoing_transactions_data)){
            return $outgoing_transactions_data;
        }
        else {
            return false;
        }
    }
    function getActiveAgentsCount(){
        $this->db->select('count(t_agents) as total_active_agents');
        $this->db->where('status',1);
        $active_agents_data = $this->db->get('t_agents')->row();
        if(count($active_agents_data)){
            return $active_agents_data->total_active_agents;
        }
        else {
            return false;
        }
    }
    function getAdminUserCount(){
        $this->db->select('count(t_admin_users) as total_admin_users');
        $this->db->where('status',1);
        $admin_users_data = $this->db->get('t_admin_users')->row();
        if(count($admin_users_data)){
            return $admin_users_data->total_admin_users;
        }
        else {
            return false;
        }
    }
    function getTotalCreditTransactions(){
        $this->db->select('sum(transaction_amount) as total_credit_transactions');
        $data = $this->db->get('t_incoming_transactions')->row();
        if(count($data)){
            return $data->total_credit_transactions;
        }
        else {
            return false;
        }
    }
    function getTotalDebitTransactions(){
        $this->db->select('sum(transaction_amount) as total_debit_transactions');
        $data = $this->db->get('t_outgoing_transactions')->row();
        if(count($data)){
            return $data->total_debit_transactions;
        }
        else {
            return false;
        }
    }

}
