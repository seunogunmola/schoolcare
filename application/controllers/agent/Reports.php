<?php

class Reports extends Agent_controller{

    function __construct() {
        parent::__construct();
    }
    function Credit_transactions(){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $where = array();
        $rules = array(
                        'studentid'=>array('field'=>'studentid','label'=>'Student Name','rules'=>'trim'),
                        'school_id'=>array('field'=>'school_id','label'=>'School Name','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE){

            $data = $this->Reports_model->array_from_post(array('studentid','school_id','account_number'));
            if ($data['studentid']!= 'All'){
                $this->data['single_student'] = TRUE;
                $where['studentid'] = $data['studentid'];
            }
            if (!empty($data['account_number'])){
                $this->data['single_student'] = TRUE;
                $where['t_incoming_transactions.studentid'] = $data['account_number'];
            }
            if ($data['school_id']!= 'All'){
                $this->data['single_school'] = TRUE;
                $where['t_schools.uniqueid'] = $data['school_id'];
            }
        }
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";
        $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
        if (!empty($where)){
            $this->db->where($where);
        }
        $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
        $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
        $this->db->order_by('t_incoming_transactions.transaction_date DESC');
        $this->data['transactions'] = $this->Incoming_Transactions_Model->get();
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $this->data['students'] = $this->Students_model->get();
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->Schools_model->get();
        $this->data['subview'] = 'Agent/Reports/credit_transactions_report';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function Debit_transactions(){
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $where = array();
        $rules = array(
                        'studentid'=>array('field'=>'studentid','label'=>'Student Name','rules'=>'trim'),
                        'school_id'=>array('field'=>'school_id','label'=>'School Name','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE){

            $data = $this->Reports_model->array_from_post(array('studentid','school_id','account_number'));
            if ($data['studentid']!= 'All'){
                $this->data['single_student'] = TRUE;
                $where['studentid'] = $data['studentid'];
            }
            if (!empty($data['account_number'])){
                $this->data['single_student'] = TRUE;
                $where['t_outgoing_transactions.studentid'] = $data['account_number'];
            }
            if ($data['school_id']!= 'All'){
                $this->data['single_school'] = TRUE;
                $where['t_schools.uniqueid'] = $data['school_id'];
            }
        }
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";
        $this->db->select('t_outgoing_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
        if (!empty($where)){
            $this->db->where($where);
        }
        $this->db->join('t_students','t_students.uniqueid = t_outgoing_transactions.studentid','inner');
        $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
        $this->db->order_by('t_outgoing_transactions.transaction_date DESC');
        $this->data['transactions'] = $this->Outgoing_Transactions_Model->get();
        $this->db->where('school_id',$this->data['agent_data']['schoolid']);
        $this->data['students'] = $this->Students_model->get();
        $this->db->order_by('t_schools.school_name ASC');
        $this->data['schools'] = $this->Schools_model->get();
        $this->data['subview'] = 'Agent/Reports/debit_transactions_report';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function Individual_student_transaction_history($studentid = NULL){
        $rules = array(
                        'uniqueid'=>array('field'=>'uniqueid','label'=>'Account Number','rules'=>'trim'),
                        'account_number'=>array('field'=>'account_number','label'=>'Account Number','rules'=>'trim'),
            );
        #PASS THE RULES TO FORM VALIDATION CLASS
        $this->form_validation->set_rules($rules) or die('I didnt set any rules');
        #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($studentid != NULL || $this->form_validation->run() == TRUE) {
                $fields = array('uniqueid','account_number');
                $data = $this->Incoming_Transactions_Model->array_from_post($fields);
                //THIS HANDLES THE SEARCH FLEXIBILITY
                //IF NON OF ACCOUNT NUMBER AND STUDENTID IS SUPPLIED THEN TRIGGER ERROR
                if (empty($data['uniqueid']) && empty($data['account_number']) && $studentid == NULL){
                    $this->session->set_flashdata('error',"The Student's Name or Account Number is required");
                    redirect('Agent/Incoming_transactions/History');
                }
                //IF ANY OF THEM IS SUPPLIED USE IT AS SEARCH PARAM
                elseif(isset($data['uniqueid']) && empty($data['account_number'])) {
                    $uniqueid = $data['uniqueid'];
                }
                elseif(empty($data['uniqueid']) && isset($data['account_number'])) {
                    $uniqueid = $data['account_number'];
                }
                elseif(!empty($studentid)) {
                    $uniqueid = $studentid;
                }
               else {
                   $uniqueid = $data['uniqueid'];
               }
                $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
                $this->db->where('t_incoming_transactions.studentid',$uniqueid);
                $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
                $this->db->join('t_schools','t_students.school_id = t_schools.uniqueid','inner');
                $this->db->order_by('t_incoming_transactions.transaction_date DESC');
                $this->data['transactions'] = $this->Incoming_Transactions_Model->get();

                if(!count($this->data['transactions'])){
                    $this->session->set_flashdata('error',"No Transactions found for selected student");
                    redirect('Agent/Incoming_transactions/History');
                }

        }
        else {
                redirect('Agent/Incoming_transactions/History');
        }
        #GET ADDITIONAL SCRIPTS AND STYLES NEEDED FOR THE VIEW
        $this->getDataTableScripts();
        $this->data['page_level_styles'] .= "<link rel='stylesheet' type='text/css' href='".getResource('js/ios-switch/switchery.css')."' />";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/switchery.js')."'></script>";
        $this->data['page_level_scripts'] .= "<script src='".getResource('js/ios-switch/ios-init.js')."'></script>";

        $this->data['students'] = $this->Students_model->get();
        $this->data['subview'] = 'Agent/Transactions/individual_student_transaction_history_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
    function print_receipt($transactionid){
        if(isset($transactionid)) {
            $this->db->select('t_incoming_transactions.*,t_students.surname,t_students.othernames,t_schools.school_name');
            $this->db->join('t_students','t_students.uniqueid = t_incoming_transactions.studentid','inner');
            $this->db->join('t_schools','t_schools.uniqueid = t_students.school_id','inner');
            $this->db->where('t_incoming_transactions.uniqueid',$transactionid);
            $this->data['transaction_data'] = $this->db->get('t_incoming_transactions')->row();
        }
        $this->data['subview'] = 'Agent/Transactions/transaction_receipt_page';
        $this->load->view('Agent/_layout_main',$this->data);
    }
}