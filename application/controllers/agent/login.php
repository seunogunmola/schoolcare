<?php
#CLASS HANDLES USER LOGIN ON THE AGENTS END
class Login extends Schoolcare_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model('Agents_Model');
    }

    function index() {
        #GET VALIDATION RULES FROM MODEL
        $rules = $this->Agents_Model->rules;
        $this->form_validation->set_rules($rules);

        #VALIDATE USER INPUT
        if($this->form_validation->run() == TRUE){
               $login_status = $this->Agents_Model->login();
               if ($login_status == true) {
                   #USER VERIFIED, REDIRECT TO DASHBOARD
                   redirect('Agent/dashboard');
               }
               else {
                   $this->data['message'] = getAlertMessage("Invalid Username/Password, Please Try Again",$type = 'danger');
               }

        }
        else {
            $this->data['message'] = (trim(validation_errors()) == FALSE ? '' : getAlertMessage(validation_errors(),$type = 'danger'));
        }

        #LOAD VIEW
        $this->load->view('Agent/login_page',$this->data);
    }

    function logout () {
        $this->Super_user_model->logout();
        redirect('Agent/login');

    }
}
