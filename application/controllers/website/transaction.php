<?php

class Transaction extends Account_controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Students_model');
        $this->load->model('website/account_model');
    }

    public function index() {

        $data['studentid'] = $this->session->userdata('accountnumber');
        $this->account_model->_primary_key = "studentid";
        $this->account_model->primary_filter = "";

        if ($this->input->post('submit')) {
            $type = $this->input->post('type');
            if ($type == "outgoing") {
                $this->account_model->_tablename = "t_outgoing_transactions";
                $this->data["transactions"] = $this->account_model->get_by($data, false);
                $this->data["type"] = "outgoing";
                $this->pagination('t_outgoing_transactions');
            } else {
                $this->account_model->_tablename = "t_incoming_transactions";
                $this->data["transactions"] = $this->account_model->get_by($data, false);
                $this->data["type"] = "incoming";
                $this->pagination('t_incoming_transactions');
            }
        } else {
            $this->account_model->_tablename = "t_outgoing_transactions";
            $this->data["transactions"] = $this->account_model->get_by($data, false);
            $this->data["type"] = "outgoing";
            $this->pagination('t_outgoing_transactions');
        }

        $this->data['subview'] = 'website/transaction';

        $this->load->view('website/_layout_main', $this->data);
    }
    
  private function pagination($table)
  {
                $this->load->library('pagination');		
		
		$config['base_url'] = site_url('website/transaction/');

		$config['per_page'] = 10; 
		$config['num_links'] = 4;
                
                $this->account_model->_tablename = $table;
                
                $data["studentid"] = $this->session->userdata('accountnumber');
                $this->db->select('count(id) as count');
		$config['total_rows'] = $this->account_model->get_by($data,true)->count;

		$config['uri_segment'] = 3;

		$config['full_tag_open'] = '<ul class="pagination1" style="list-style:none;">';
		$config['full_tag_close'] = '</ul>';

		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="first">';
		$config['first_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';


		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="previous">';
		$config['prev_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="last" >';
		$config['last_tag_close'] = '</li>';
				  
		$this->pagination->initialize($config); 

		$this->data["pagination"] =  $this->pagination->create_links();
      
  }

}
