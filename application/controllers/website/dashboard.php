<?php

class Dashboard extends Account_controller {

    function __construct(){
         parent::__construct();
     
         $this->load->model('Students_model');
		 $this->load->model('website/account_model');
    }

    public function index($id=NULL){
	     
        $data["uniqueid"] = $this->session->userdata('accountnumber');
				
        $student = $this->Students_model->get_by($data,true);

		if(!count($student))
		 {
		    $this->session->set_flashdata('error','Invalid account details');
            redirect('website/authenticate');				   
		 }

		//$this->Students_model->_primary_key = "studentid";
        $this->account_model->_tablename = "t_outgoing_transactions";
        $this->account_model->primary_filter = "strval";
		
		$account["studentid"] = $this->session->userdata('accountnumber');
		$this->db->select('sum(transaction_amount) as total');
		$withdrawals = $this->account_model->get_by($account,true)->total;
		
		$this->account_model->_tablename = "t_incoming_transactions";
		$this->db->select('sum(transaction_amount) as total');
		$deposits = $this->account_model->get_by($account,true)->total;
		
		$this->data['balance'] = intval($deposits) - intval($withdrawals);
		
		$this->data["student"] = $student;
		
	    $this->data['subview'] = 'website/dashboard';

	    $this->load->view('website/_layout_main', $this->data);
		
    }
	
	
 public function paymentslip()
 {
	 
	    if($this->input->post('amount')){
		
	    $data["uniqueid"] = $this->session->userdata('accountnumber');
				
        $student = $this->Students_model->get_by($data,true);
		
		$this->data["student"] = $student;
		
		$this->data["amount"] = $this->input->post('amount');
		
        $this->data['subview'] = 'website/paymentslip';

	    $this->load->view('website/_layout_main', $this->data);	
		}
		else
		{
			redirect('website/dashboard');
		}
 }


}
