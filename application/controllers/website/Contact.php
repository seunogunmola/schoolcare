<?php

class Contact extends Schoolcare_Controller {
    function __construct() {
        parent::__construct();
    }


    function index(){
        $rules = array ('sender_name'=>array('field'=>'sender_name',
                                            'label'=>'Sender Name',
                                            'rules'=>'trim|required'),
                         'sender_email'=>array('field'=>'sender_email',
                                            'label'=>'Your Email',
                                            'rules'=>'trim|required|valid_email'),
                        'sender_phone_number'=>array('field'=>'sender_phone_number',
                                            'label'=>'Your Phone Number',
                                            'rules'=>'trim|required'),
                        'message'=>array('field'=>'message',
                                            'label'=>'Message',
                                            'rules'=>'trim|required'));
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
            $data['sender_name'] = $this->input->post('sender_name');
            $data['sender_email'] = $this->input->post('sender_email');
            $data['sender_phone_number'] = $this->input->post('sender_phone_number');
            $data['message'] = $this->input->post('message');
            $data['status'] =0;
            $data['date_sent'] = date('Y-m-d H:i:s');
           // $this->website_model->sendemail($email_string);

            $this->db->set($data);
            $saved = $this->db->insert('t_enquiries');
            if ($saved){
                $email_string = "An Enquiry was just made on Schoolcare.com see details"
                    ."\nName :".$data['sender_name']
                    ."\nEmail :".$data['sender_email']
                    ."\nPhone Number :".$data['sender_phone_number']
                    ."\nMessage :".$data['message']
                    ."\nDate Sent :".$data['date_sent'];
                $sms_string = "A New Request was received on platformtechltd.com \n Check email or log on to admin end to see details";
                sendsms($sms_string);
                $this->session->set_flashdata('msg',"Your Message was sent Successfully");
                redirect('website/contact');
            }
            else{
                $this->session->set_flashdata('error', "An Error Occured, Please try again later");
                redirect('website/contact');
            }
        }

        $this->data['subview'] = 'website/_templates/contact';		// extra view is an array of standard views you want to load
        $this->load->view('website/_layout_main', $this->data);
    }


}
