<?php

class Logout extends Website_Controller {

    function __construct(){
         parent::__construct();
        //$this->load->model('hub/home_model');
         $this->load->model('Students_model');
         $this->load->model('Schools_model');
    }

    public function index($id=NULL){
      
	     $array_items = array('accountnumber' => '', 'loggedin' => '');
         $this->session->unset_userdata($array_items);
		 
		 redirect('website/authenticate');
		
    }


}
