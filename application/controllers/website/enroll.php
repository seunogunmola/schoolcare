<?php

class Enroll extends Website_Controller {

    function __construct() {
		
          parent::__construct();
        
        //$this->load->model('hub/home_model');
         $this->load->model('Students_model');
         $this->load->model('Schools_model');
    }

    public function index($id=NULL){
      
	 
	  $rules = array(
        'surname' => array(
                'field' => 'surname',
                'label' => 'surname',
                'rules' => 'trim|required'
        ),
        'othernames' => array(
                'field' => 'othernames',
                'label' => 'Othernames',
                'rules' => 'trim|required'
        ),
        'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
        ),
        'date_of_birth' => array(
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
        ),
        'school_id' => array(
                'field' => 'school_id',
                'label' => 'School',
                'rules' => 'trim|required'
        ),
        'parent_name' => array(
                'field' => 'parent_name',
                'label' => 'Parent Name',
                'rules' => 'trim|required'
        ),
        'parent_email_address' => array(
                'field' => 'parent_email_address',
                'label' => 'Parent Email',
                'rules' => 'trim'
        ),
        'parent_phone_number' => array(
                'field' => 'parent_phone_number',
                'label' => 'Parent Phone',
                'rules' => 'trim|required'
        ),
        );
	
	     if($this->input->post("submit")):
		 
	     $this->form_validation->set_rules($rules);
	    #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
        if ($this->form_validation->run() == TRUE) {
                $fields = array('surname','othernames','gender','date_of_birth','school_id','parent_name','parent_email_address','parent_phone_number','status');
                $data = $this->Students_model->array_from_post($fields);
                if($id == NULL){
                    $data['uniqueid'] = $this->Students_model->generate_unique_id($len = 10,$fieldname = "uniqueid");
                    $data['passcode'] = $this->Students_model->hash('1234');
                }
                if(isset($_FILES['passport']) && $_FILES['passport']['size'] > 0) {
                    $data['passport'] = $this->_handlePassportUpload();
                }
                else if (!isset($_FILES['passport']) && $id ==  NULL){
                    $data['passport'] = './resources/uploads/images/students_passports/no_image.jpg';
                }
                $this->Students_model->save($data,$id);
                $this->session->set_flashdata('msg','Student Enrolled Succesfully');
			
                redirect('website/enroll/step1');

        }
      else{
	
           $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
	
         }
	   
	   endif;
	   
	   
	   $this->step1();
		
    }
	
	
	 public function step1(){
		 
	    $this->db->order_by('t_schools.school_name ASC');
	    $this->data['schools'] = $this->Schools_model->get();
		
        $this->data['subview'] = 'website/enroll_step1';

        $this->load->view('website/_layout_main', $this->data);
		
    }
	
	public function step2(){
		
		if($this->input->post("continue"))
		{
			$this->data["school_id"] = $this->input->post('school_id');
			
	    }
		
		$this->data['subview'] = 'website/enroll_step2';

	    $this->load->view('website/_layout_main', $this->data);
	     
    }
	

 private function _handlePassportUpload(){
        if (isset($_FILES['passport']) && $_FILES['passport']['size'] > 0) {
            $upload_location = '';
            $upload_path ='./resources/uploads/images/students_passports/';
            if (!is_dir($upload_path))
                mkdir ($upload_path, 0777);
                $source_file_name = 'passport';
                $destination_file_name = mt_rand($min = 0000000000, $max = 999999999);
                @$name = $_FILES[$source_file_name]["name"];
                @$ext = end((explode(".", $name)));
                if ($this->perform_upload($upload_path,$source_file_name,$destination_file_name))
                    {
                        $upload_location = $upload_path.$destination_file_name.".".$ext;
                    }
                else {
					    $this->sesssion->set_flashdata("student",$this->input->post());
                        $this->session->set_flashdata('error',$this->data['upload_error']['error']);
                        redirect('website/enroll/step1');
                    }
            }
            return $upload_location;
    }
	
}
