<?php

class Authenticate extends Website_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('hub/home_model');
        $this->load->model('Students_model');
        $this->load->model('Schools_model');
    }

    public function index($id = NULL) {

        $rules = array(
            'uniqueid' => array(
                'field' => 'uniqueid',
                'label' => 'Account Number',
                'rules' => 'trim|required'
            ),
            'passcode' => array(
                'field' => 'passcode',
                'label' => 'Pass Code',
                'rules' => 'trim|required'
            ),
        );

        if ($this->input->post("submit")):

            $this->form_validation->set_rules($rules);
            #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
            if ($this->form_validation->run() == TRUE) {
                $fields = array('uniqueid', 'passcode');
                $data = $this->Students_model->array_from_post($fields);
                $student = $this->Students_model->get_by($data, true);

                if(!count($student)) {
                    $this->session->set_flashdata('error', 'Invalid account details');
                    redirect('website/authenticate');
                } else {
                    $this->session->set_userdata("accountnumber", $student->uniqueid);
                    $this->session->set_userdata("loggedin", true);
                    redirect('website/dashboard');
                }
            } else {
                $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }

        endif;

        $this->data['subview'] = 'website/login';

        $this->load->view('website/_layout_main', $this->data);
    }

}
