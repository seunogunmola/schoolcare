<?php

class Settings extends Account_controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Students_model');
        $this->load->model('website/account_model');
    }

    public function index() {

        $id = $this->session->userdata('accountnumber');
        $this->account_model->_primary_key = "uniqueid";
        $this->account_model->_tablename = "t_students";
        $this->account_model->primary_filter = "strval";

        $rules = array(
            'surname' => array(
                'field' => 'surname',
                'label' => 'surname',
                'rules' => 'trim|required'
            ),
            'othernames' => array(
                'field' => 'othernames',
                'label' => 'Othernames',
                'rules' => 'trim|required'
            ),
            'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
            ),
            'date_of_birth' => array(
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
            ),
            'parent_name' => array(
                'field' => 'parent_name',
                'label' => 'Parent Name',
                'rules' => 'trim|required'
            ),
            'parent_email_address' => array(
                'field' => 'parent_email_address',
                'label' => 'Parent Email',
                'rules' => 'trim'
            ),
            'parent_phone_number' => array(
                'field' => 'parent_phone_number',
                'label' => 'Parent Phone',
                'rules' => 'trim|required'
            ),
        );

        if ($this->input->post("submit")):

            $this->form_validation->set_rules($rules);
            #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
            if ($this->form_validation->run() == TRUE) {
                $fields = array('surname', 'othernames', 'gender', 'date_of_birth', 'parent_name', 'parent_email_address', 'parent_phone_number');

                $data = $this->Students_model->array_from_post($fields);

                if ($id == NULL) {
                    
                }
                if (isset($_FILES['passport']) && $_FILES['passport']['size'] > 0) {
                    $data['passport'] = $this->_handlePassportUpload();
                } else if (!isset($_FILES['passport']) && $id == NULL) {
                    $data['passport'] = './resources/uploads/images/students_passports/no_image.jpg';
                }

                $this->account_model->save($data, $id);
                $this->session->set_flashdata('msg', 'Account details updated successfully');

                redirect('website/settings');
            } else {

                $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }
        endif;

        $data["uniqueid"] = $id;

        $this->data["student"] = $this->account_model->get_by($data, true);

        $this->data['subview'] = 'website/settings';

        $this->load->view('website/_layout_main', $this->data);
    }

}
