<?php

class Passcode extends Account_controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Students_model');
        $this->load->model('website/account_model');
    }

    public function index() {

        $id = $this->session->userdata('accountnumber');
        $this->account_model->_primary_key = "uniqueid";
        $this->account_model->_tablename = "t_students";
        $this->account_model->primary_filter = "strval";

        $rules = array(
            'passcode' => array(
                'field' => 'passcode',
                'label' => 'Passcode',
                'rules' => 'trim|required'
            ),
            'confirm_passcode' => array(
                'field' => 'passcode',
                'label' => 'Passcode Confirmation',
                'rules' => 'trim|required'
            ),
        );

        if ($this->input->post("submit")):
            $this->form_validation->set_rules($rules);
            #CHECK IF FORM WAS VALIDATED SUCCESSFULLY
            if ($this->form_validation->run() == TRUE) {

                $fields = array('passcode');

                $data = $this->account_model->array_from_post($fields);

                if ($this->account_model->save($data, $id)) {
                    $this->session->set_flashdata('msg', 'Passcode changed successfully');
                } else {
                    $this->session->set_flashdata('error', 'Error while updating passcode, try again');
                }

                redirect('website/passcode');
            } else {
                $this->data['message'] = TRIM(validation_errors()) != FALSE ? getAlertMessage(validation_errors()) : '';
            }
        endif;

        $data["uniqueid"] = $id;

        $this->data["student"] = $this->account_model->get_by($data, true);

        $this->data['subview'] = 'website/change_passcode';

        $this->load->view('website/_layout_main', $this->data);
    }

}
