<?php

class Home extends Website_Controller {

    function __construct() {
        parent::__construct();
        
        //$this->load->model('hub/home_model');
    }

    public function index(){

        $this->data['subview'] = 'website/_templates/testimonial';		// extra view is an array of standard views you want to load 
	$this->data['extraview'][]= 'website/_templates/about';
        $this->data['extraview'][] = 'website/_templates/bannerbottom';	
	//$this->data['extraview'][]= 'website/_templates/testimonial';
        $this->load->view('website/_layout_main', $this->data);
		
    }

}
