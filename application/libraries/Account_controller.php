<?php

class Account_controller extends Schoolcare_Controller {

    function __construct() {

        parent::__construct();

        if (!$this->session->userdata('loggedin')) {
            redirect('website/authenticate');
        }
    }

}
