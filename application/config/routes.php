<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'Welcome';
$route['agent'] = 'agent/login';
$route['admin'] = 'administrator/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
