<?php

class Enquiries_model extends Schoolcare_Model{
    protected $_tablename = 't_enquiries';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_enquiries.date_sent DESC';
    
    function __construct() {
        parent::__construct();
    }

}
