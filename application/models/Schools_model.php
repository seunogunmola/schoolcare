<?php

class Schools_model extends Schoolcare_Model{
    protected $_tablename = 't_schools';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_schools.id ASC';
    public $rules = array(
        'school_name' => array(
                'field' => 'school_name',
                'label' => 'School Name',
                'rules' => 'trim|required'
        ),
        'school_address' => array(
                'field' => 'school_address',
                'label' => 'School Address',
                'rules' => 'trim'
        ),
        'school_phone' => array(
                'field' => 'school_phone',
                'label' => 'School Phone',
                'rules' => 'trim|required'
        ),
        'school_email' => array(
                'field' => 'school_email',
                'label' => 'School Email',
                'rules' => 'trim'
        ),
        'school_state' => array(
                'field' => 'school_state',
                'label' => 'School State',
                'rules' => 'trim|required'
        ),
        'school_country' => array(
                'field' => 'school_country',
                'label' => 'School Country',
                'rules' => 'trim|required'
        ),
        );

    function __construct() {
        parent::__construct();
    }

    function get_new() {
        $std = new stdClass();
        $std->school_name = '';
        $std->school_address = '';
        $std->school_phone = '';
        $std->school_email = '';
        $std->school_state = '';
        $std->school_country = '';
        $std->uniqueid = '';
        $std->status = '';
        return $std;
    }

}
