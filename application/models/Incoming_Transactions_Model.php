<?php

class Incoming_Transactions_Model extends Schoolcare_Model{
    protected $_tablename = 't_incoming_transactions';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_incoming_transactions.id ASC';
    public $rules = array(
        'surname' => array(
                'field' => 'surname',
                'label' => 'surname',
                'rules' => 'trim|required'
        ),
        'othernames' => array(
                'field' => 'othernames',
                'label' => 'Othernames',
                'rules' => 'trim|required'
        ),
        'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
        ),
        'date_of_birth' => array(
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
        ),
        'school_id' => array(
                'field' => 'school_id',
                'label' => 'School',
                'rules' => 'trim|required'
        ),
        'parent_name' => array(
                'field' => 'parent_name',
                'label' => 'Parent Name',
                'rules' => 'trim|required'
        ),
        'parent_email_address' => array(
                'field' => 'parent_email_address',
                'label' => 'Parent Email',
                'rules' => 'trim'
        ),
        'parent_phone_number' => array(
                'field' => 'parent_phone_number',
                'label' => 'Parent Phone',
                'rules' => 'trim|required'
        ),
        );
    function __construct() {
        parent::__construct();
    }

    function Login() {
        $username = $this->input->post('username');
        $password = $this->hash($this->input->post('password'));

        $where = "((username = '$username') OR (email_address = '$username')) AND password = '$password'";
        $user = $this->get_by($where,TRUE);
        if (count($user)) {
            $data = array();

                $data['current_userid']= $user->id;
                $data['current_user_uniqueid']= $user->uniqueid;
                $data['username']= $user->username;
                $data['fullname']= $user->fullname;
                $data['loggedin']= TRUE;


            $this->session->set_userdata($data);
            return true;
        }
        else{
            return false;
        }
    }

    function loggedin() {
        return (bool)$this->session->userdata('loggedin');
    }

    function logout () {
        $this->session->sess_destroy();
    }
    //INITIALISE A NEW USER
    function get_new() {
        $user = new stdClass();
        $user->surname = '';
        $user->othernames = '';
        $user->gender = '';
        $user->date_of_birth = '';
        $user->passport = '';
        $user->school_id = '';
        $user->parent_name = '';
        $user->parent_email_address = '';
        $user->parent_phone_number = '';
        $user->status = 1;
        return $user;
    }



}
