<?php

class Migration_Create_Student_Table extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'uniqueid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'passcode' => array(
                                'type' => 'TEXT',
                        ),
                        'surname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'othernames' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'gender' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '6',
                        ),
                        'date_of_birth' => array(
                                'type' => 'DATE',
                        ),
                        'passport' => array(
                                'type' => 'TEXT',
                                'NULL' => TRUE,
                        ),
                        'school_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'parent_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'parent_email_address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'NULL' => TRUE,
                        ),
                        'parent_phone_number' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '11',
                                'NULL' => TRUE,
                        ),
                        'status' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp',

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_students');
                }

    public function down(){
                $this->dbforge->drop_table('t_students');
        }
}
