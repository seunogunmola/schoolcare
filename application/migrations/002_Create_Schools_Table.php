<?php

class Migration_Create_Schools_Table extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'uniqueid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'school_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'school_address' => array(
                                'type' => 'TEXT'
                        ),
                        'school_phone' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'school_email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'school_state' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'school_country' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'status' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                        ),
                        'added_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp',

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_schools');
                }

    public function down(){
                $this->dbforge->drop_table('t_schools');
        }
}
