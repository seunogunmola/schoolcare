<?php

class Migration_Create_Enquiries_Table extends CI_Migration {
    function __construct() {
        parent::__construct();
    }
    
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'sender_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'sender_email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'sender_phone_number' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '11',
                        ),
                        'message' => array(
                                'type' => 'TEXT',
                        ),
                        'date_sent' => array(
                                'type' => 'TIMESTAMP',
                        ),
                        'status' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                        ),

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_enquiries');
                }

    public function down(){
                $this->dbforge->drop_table('t_enquiries');
        }
}
