<?php

class Migration_Create_Incoming_Transactions_Table extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'uniqueid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'studentid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'transaction_amount' => array(
                                'type' => 'DECIMAL',
                                'constraint' => '12,2',
                        ),
                        'transaction_date' => array(
                                'type' => 'DATE',
                        ),
                        'transaction_made_by' => array(
                                'type' => 'VARCHAR',
                        ),
                        'transaction_logged_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '12',
                        ),
                        'transaction_logged_date' => array(
                                'type' => 'DATE',
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp',

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_incoming_transactions');
                }

    public function down(){
                $this->dbforge->drop_table('t_incoming_transactions');
        }
}
