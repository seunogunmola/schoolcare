<?php

class Schoolcare_Controller extends CI_Controller {

    public $data = array();
    function __construct() {
        parent::__construct();
        #GET APP DATA
        $this->data['errors']= '';
        $this->data['sitename']= config_item('sitename');
        $this->data['siteslug']= config_item('siteslug');
        $this->data['currency_symbol']= 'Pts';

        #PROCESS URL
        $url = current_url();
        $current_url = str_replace(uri_string() . config_item('url_suffix'), '', $url);
        $this->data['current_url'] = $cur_url = $current_url = rtrim($current_url, '/');
        $this->data['current_url'] = ((trim(config_item('index_page')) != false) ? str_replace(config_item('index_page'), '', $cur_url) : $cur_url);
        $this->data['current_url'] = $current_url = str_replace('www.', '', $this->data['current_url']);
        $this->data['current_url'] = rtrim($current_url, '/');

        #GLOBAL VARIABLES FOR PAGE LEVEL STYLES AND SCRIPTS
        $this->data['page_level_styles'] = "";
        $this->data['page_level_scripts'] = "";
        #RUN ANY NEW MIGRATIONS
        $this->load->library('migration');
        $this->migration->current();

        @$current_user_session = $this->session->userdata['current_user_uniqueid'];
        if(isset($current_user_session))
               $this->data['current_user_session'] = $current_user_session;
    }

   //SINCE MORE THAN ONE METHOD IS MAKING USE OF THE SAME SCRIPTS IT IS GOOD PRACTICE FROM KEEP IT IN ONE PLACE
    public function getDataTableScripts() {
        #scripts for the dataTable
        $this->data['page_level_scripts'] = '<script src="' . getResource("js/advanced-datatable/js/jquery.dataTables.js") . '" ></script>';
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/data-tables/DT_bootstrap.js") . '" ></script>';
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/dynamic_table_init.js") . '" ></script>';
    }

}
