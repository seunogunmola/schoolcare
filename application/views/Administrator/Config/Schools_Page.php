s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    <span class="fa fa-institution"></span> Manage Schools
                </h3>
                <a href = "#existing_schools"> <button class="btn btn-primary"> <span class="fa fa-chevron-circle-down"></span> Existing Schools </button> </a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo!empty($school->school_name) ? '<i class = "fa fa-edit"></i> Editing School : ' . $school->school_name : '<i class = "fa fa-plus"></i> Add New School' ?>
                            </div>
                            <div class="panel-body">
                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <?php echo $message ?>
                                    <?php echo form_open($action = ''); ?>
                                <div class="col-md-6">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>School Name</label>
                                            <input class="form-control"  required="" name = "school_name" type = "text" placeholder="Enter School Name" value = "<?php echo set_value('school_name', $school->school_name) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>School Phone</label>
                                            <input class="form-control"  required="" name = "school_phone" type = "text" placeholder="Enter School Phone" value = "<?php echo set_value('school_phone', $school->school_phone) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>School Email</label>
                                            <input class="form-control"   name = "school_email" type = "text" placeholder="Enter School Email Address" value = "<?php echo set_value('school_email', $school->school_email) ?>">
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>School Address</label>
                                            <textarea name="school_address" class="form-control"><?php echo set_value($school->school_address) ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>School State</label>
                                            <?php
                                                $states_array = getStates();
                                                $status_options = array();
                                                $status_options[''] = 'Select State';
                                                foreach($states_array as $state){
                                                    $status_options[$state] = $state;
                                                }
                                                echo form_dropdown('school_state', $status_options, set_value('school_state',$school->school_state),'class = "form-control" required = "required"');
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>School State</label>
                                            <?php
                                                $country_array = getCountries();
                                                $country_options = array();
                                                $country_options[''] = 'Select Country';
                                                foreach($country_array as $country){
                                                    $country_options[$country] = $country;
                                                }
                                                echo form_dropdown('school_country', $country_options, set_value('school_country',$school->school_country),'class = "form-control" required = "required"');
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Activation Status</label>
                                            <?php

                                                $status_array = array();
                                                $status_array['1'] = 'Enabled';
                                                $status_array['0'] = 'Disabled';
                                                echo form_dropdown('status', $status_array, set_value('status',$school->status),'class = "form-control" required = "required"');
                                            ?>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-right"> <span class="fa fa-plus"></span> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a id ="existing_schools"></a>
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <i class = "fa fa-list"></i> Existing Schools
                            </div>
                            <div class="panel-body">
                                <div class="row">
<?php if(count($schools)) { ?>
                                    <div class="adv-table" style="padding: 5px">
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th> School Name</th>
                                                <th> School Phone</th>
                                                <th> School State</th>
                                                <th> Added By</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php $sn = 1;
                                            foreach ($schools as $school):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>

                                                    <td> <?php echo ucwords($school->school_name); ?> </td>
                                                    <td> <?php echo ucwords($school->school_phone); ?> </td>
                                                    <td> <?php echo ucwords($school->school_state); ?> </td>
                                                    <td> <?php echo ucwords($school->created_by); ?> </td>
                                                    <td>
                                                        <?php echo getBtn('Schools/'. $school->id.'/0', 'edit') ?>
                                                        <?php echo getBtn('Schools/' . $school->id.'/1', 'delete') ?>
                                                    </td>
                                                </tr>
                                                <?php $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
<?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--body wrapper end-->

