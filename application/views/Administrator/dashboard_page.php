

<body class="sticky-header">

    <section>
        <!-- main content start-->
        <div class="main-content" >
            <!-- page heading start-->
            <div class="page-heading">
                <h3>
                    Dashboard
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="active"> My Dashboard </li>
                </ul>
            </div>
            <!-- page heading end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <!--statistics start-->
                        <div class="row state-overview">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Students/view') ?>" style="color:white">
                                    <div class="panel purple">
                                        <div class="symbol">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($student_count) ? $student_count : '' ?> </div>
                                            <div class="title">Students Enrolled</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Config/Schools') ?>" style="color:white">
                                    <div class="panel red">
                                        <div class="symbol">
                                            <i class="fa fa-institution"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($school_count) ? $school_count : '' ?> </div>
                                            <div class="title">Schools Enrolled</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row state-overview">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Incoming_transactions/History') ?>" style="color:white">
                                    <div class="panel blue">
                                        <div class="symbol">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($total_credit_transactions_count) ? $total_credit_transactions_count : '' ?> </div>
                                            <div class="title">Credit Transactions</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Outgoing_transactions/History') ?>" style="color:white">
                                    <div class="panel orange">
                                        <div class="symbol">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($total_debit_transactions_count) ? $total_debit_transactions_count : '' ?> </div>
                                            <div class="title">Debit Transactions</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--statistics end-->
                    </div>
                    <div class="col-md-6">
                        <!--more statistics box start-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Quick Search
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'success') : '' ?>
                                        <?php echo form_open('Administrator/Dashboard/quick_search'); ?>
                                        <div class="form-group">
                                            <label class="col-md-12">Student Name</label>
                                            <div class="col-md-12">
                                                <?php
                                                $students_array = array();
                                                $students_array[] = 'Select Student';
                                                if (count($students)) {
                                                    foreach ($students as $student):
                                                        $students_array[$student->uniqueid] = $student->surname . ' ' . $student->othernames;
                                                    endforeach;
                                                }
                                                echo form_dropdown('uniqueid', $options = $students_array, set_value('studentid'), 'class = "form-control" required = "required"');
                                                ?>
                                            </div>
                                            <label class="col-md-12">or Account Number</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" name = "account_number">
                                            </div>
                                            <div class="col-md-12">
                                                <br/>
                                                <button class="btn btn-primary"> <span class="fa fa-search"> </span> Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                        <!--more statistics box end-->
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <!--statistics start-->
                        <div class="row state-overview">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Agents') ?>" style="color:white">
                                    <div class="panel red">
                                        <div class="symbol">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value">  <?= isset($active_agents_count) ? $active_agents_count : '' ?> </div>
                                            <div class="title">Active Agents</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Users') ?>" style="color:white">
                                    <div class="panel purple">
                                        <div class="symbol">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($admin_user_count) ? $admin_user_count : '' ?> </div>
                                            <div class="title">Admin Users</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!--statistics end-->
                    </div>
                    <div class="col-md-6">
                        <!--statistics start-->
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Incoming_transactions') ?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Total Credit Transactions</div>
                                        <div class="panel-body">
                                            <center> <h3> <?= isset($total_credit_transactions) ?$currency_symbol.' '.$total_credit_transactions : '0.00' ?> </h3> </center>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('Administrator/Outgoing_transactions') ?>">
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">Total Debit Transactions</div>
                                        <div class="panel-body">
                                            <center> <h3> <?= isset($total_debit_transactions) ?$currency_symbol.' '.$total_debit_transactions : '0.00' ?> </h3> </center>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!--statistics end-->
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class ="panel-heading">
                                <h4 class = "alert alert-info">Most Recent Credit Transactions</h4>
                            </div>
                            <div class="panel-body">
                                <?php if (isset($recent_credit_transactions) && !empty($recent_credit_transactions)) { ?>
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <td> S/N</td>
                                                <td> Student Name</td>
                                                <td> Amount</td>
                                                <td> Date</td>
                                                <td> Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $sn = 1;
                                                foreach($recent_credit_transactions as $credit_transaction): ?>
                                            <tr>
                                                <td> <?php echo $sn; ?></td>
                                                <td> <?php echo $credit_transaction->surname.' '.$credit_transaction->othernames; ?></td>
                                                <td> <?php echo $currency_symbol.' '.$credit_transaction->transaction_amount ?></td>
                                                <td> <?php echo $credit_transaction->transaction_date ?></td>
                                                <td> <a href="<?php echo site_url('Administrator/Incoming_transactions/print_receipt/'.$credit_transaction->uniqueid) ?>" target="_blank"> <span class="label label-info"> Full Details</span> </a></td>
                                            </tr>
                                            <?php
                                                $sn++;
                                                endforeach;?>
                                        </tbody>
                                    </table>
                                <?php }
                                else {
                                    ?>
                                    <div class="alert alert-info"> No Credit Transactions Recorded Yet</div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class ="panel-heading">
                                <h4 class = "alert alert-danger">Most Recent Debit Transactions</h4>
                            </div>
                            <div class="panel-body">

                                <?php
                                if (isset($recent_debit_transactions) && !empty($recent_debit_transactions)) { ?>
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <td> S/N</td>
                                                <td> Student Name</td>
                                                <td> Amount</td>
                                                <td> Date</td>
                                                <td> Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $sn = 1;
                                                foreach($recent_debit_transactions as $debit_transaction): ?>
                                            <tr>
                                                <td> <?php echo $sn; ?></td>
                                                <td> <?php echo $debit_transaction->surname.' '.$debit_transaction->othernames; ?></td>
                                                <td> <?php echo $currency_symbol.' '.$debit_transaction->transaction_amount ?></td>
                                                <td> <?php echo $debit_transaction->transaction_date ?></td>
                                                <td> <a href="<?php echo site_url('Administrator/Outgoing_transactions/print_receipt/'.$debit_transaction->uniqueid) ?>" target="_blank"> <span class="label label-info"> Full Details</span> </a></td>
                                            </tr>
                                            <?php
                                                $sn++;
                                                endforeach;?>
                                        </tbody>
                                    </table>
                                <?php }
                                else {
                                    ?>
                                    <div class="alert alert-info"> No Debit Transactions Recorded Yet</div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (isset($recent_credit_transactions) && !empty($recent_credit_transactions)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class ="panel-heading">
                                <h4 class = "alert alert-info">Incoming Transactions History</h4>
                            </div>
                            <div class="panel-body">
                                <div id="incoming_transactions" style="height: 250px;"></div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
<?php if (isset($recent_debit_transactions) && !empty($recent_debit_transactions)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class ="panel-heading">
                                <h4 class = "alert alert-danger">Outgoing Transactions History</h4>
                            </div>
                            <div class="panel-body">
                                <div id="outgoing_transactions" style="height: 250px;"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--body wrapper end-->
<?php } ?>
            <script type="text/javascript">
                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'incoming_transactions',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: [<?php echo $incoming_transactions_data; ?>
                    ],
                    // The name of the data record attribute that contains x-values.
                    xkey: 'date',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Value']
                });
            </script>
            <script type="text/javascript">
                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'outgoing_transactions',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: [<?php echo $outgoing_transactions_data; ?>
                    ],
                    // The name of the data record attribute that contains x-values.
                    xkey: 'date',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Value']
                });
            </script>