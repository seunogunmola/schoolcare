            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="<?php echo site_url('Administrator/dashboard') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-list"><a href=""><i class="fa fa-user"></i> <span>Manage Students</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Students')?>"> Enrol Student</a></li>
                        <li><a href="<?php echo site_url('Administrator/Students/view')?>"> View Enrolled Students</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-money"></i> <span>Credit Wallet</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Incoming_transactions')?>"> Fund Wallet</a></li>
                        <li><a href="<?php echo site_url('Administrator/Incoming_transactions/History')?>"> Credit Transactions History</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-money"></i> <span>Debit Wallet</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Outgoing_transactions')?>"> Debit Wallet</a></li>
                        <li><a href="<?php echo site_url('Administrator/Outgoing_transactions/History')?>"> Debit Transactions History</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-bar-chart"></i> <span>Reports</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Reports/Credit_transactions')?>"> Credit Transactions Report</a></li>
                        <li><a href="<?php echo site_url('Administrator/Reports/Debit_transactions')?>"> Debit Transactions Report</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-cogs"></i> <span>Config</span></a>
                    <ul class="sub-menu-list">

                        <li> <a href="<?php echo site_url('Administrator/Config/Schools')?>"> <i class="fa fa-institution"></i> Manage Schools</a></li>

                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-comment"></i> <span>Enquiries</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Enquiries')?>"> View Enquries</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Manage Agents</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Agents')?>"> Create Agent</a></li>
                        <li><a href="<?php echo site_url('Administrator/Agents/#existing_users')?>"> View Agents</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Manage Admin Users</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="<?php echo site_url('Administrator/Users')?>"> Create User</a></li>
                        <li><a href="<?php echo site_url('Administrator/Users/#existing_users')?>"> View Users</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>