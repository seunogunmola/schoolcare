s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Transaction Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Outgoing (Debit) Transaction History</a>
                    </li>
                    <li class="active"> <a href="#existing_users">  </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a id ="existing_users"></a>
                            Debit Transactions History
                        </div>
                        <?php
                        if (count($transactions)) {
                            ?>
                            <div class="panel-body">
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="alert alert-info"> View Individual Student Transaction History</h4>
                                        <?php echo form_open('Administrator/Outgoing_transactions/Individual_student_transaction_history','target = "_blank"');?>
                                        <div class="form-group">
                                            <label class="col-md-2">Student Name</label>
                                            <div class="col-md-3">
                                                <?php
                                                    $students_array = array();
                                                    $students_array[] = 'Select Student';
                                                    if(count($students)){
                                                        foreach ($students as $student):
                                                            $students_array[$student->uniqueid] = $student->surname.' '.$student->othernames;
                                                        endforeach;
                                                    }
                                                    echo form_dropdown('uniqueid',$options = $students_array,  set_value('studentid'),'class = "form-control" required = "required"');
                                                ?>
                                            </div>
                                            <label class="col-md-2">or Account Number</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" name = "account_number">
                                            </div>
                                            <button class="btn btn-primary"> <span class="fa fa-search"> </span> Proceed</button>
                                        </div>
                                    </div>
                                   <?php echo form_close(); ?>
                            </div>
                                <div class="table-responsive">
                                    <h4 class="alert alert-success"> All Transactions History</h4>
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th> Beneficiary </th>
                                                <th> School Name </th>
                                                <th> Transaction Amount </th>
                                                <th> Transaction Date </th>
                                                <th> Transaction Purpose </th>
                                                <th> Transaction Receipient</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            foreach ($transactions as $value):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>
                                                    <td> <?php echo $value->surname.' '.$value->othernames; ?> </td>
                                                    <td> <?php echo $value->school_name; ?> </td>
                                                    <td> <?php echo $value->transaction_amount; ?> </td>
                                                    <td> <?php echo $value->transaction_date; ?> </td>
                                                    <td> <?php echo $value->transaction_purpose; ?> </td>
                                                    <td> <?php echo $value->transaction_recepient; ?> </td>
                                                    <td>
                                                        <div class="dropdown">
                                                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                          <span class="caret"></span></button>
                                                          <ul class="dropdown-menu">
                                                                <li> <a href="<?php echo site_url('Administrator/Outgoing_transactions/edit_debit_wallet/'.$value->uniqueid) ?>"> Edit Transaction</a></li>
                                                                <li> <a onclick="return(confirm('Are you sure you want to delete? \n It cannot be undone o'))" href="<?php echo site_url('Administrator/Outgoing_transactions/delete/'.$value->uniqueid) ?>"> Delete Transaction</a></li>
                                                                <li> <a href="<?php echo site_url('Administrator/Outgoing_transactions/print_receipt/'.$value->uniqueid) ?>" target="_blank"> Print Transaction Receipt </a></li>
                                                          </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <?php
                        }
                        else {
                            echo getAlertMessage('Sorry : You dont have any registered users yet');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->

