
<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                     Transaction Receipt
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 Transaction Receipt
                            </div>
                            <div class="panel-body">
                                <div class="row" id = "printArea">
                                    <center>
                                        <table class="" width = "600px" border = "1" style="border: #000 2px; border-collapse: collapse" >
                                            <tr>
                                                <td colspan="3">   <center> <img src="<?php echo getResource('images/login_logo.png') ?>" style="max-width:200px" alt=""/> </center></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">  <center> <h4> Debit Transaction Receipt </center></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td align = "right"> <i> Receipt Number :  <?php echo $transaction_data->uniqueid ?> </i></td>
                                            </tr>
                                            <tr>
                                                <td>Beneficiary Name</td>
                                                <td> <strong> <?php echo ucfirst($string = strtolower($transaction_data->surname)).' '.  ucfirst($string = strtolower($transaction_data->surname)) ?> </strong></td>
                                            </tr>
                                            <tr>
                                                <td>Account Number</td>
                                                <td> <strong style="color:crimson"> <?php echo $transaction_data->studentid ?> </strong></td>
                                            </tr>
                                            <tr>
                                                <td>School Name</td>
                                                <td> <?php echo $transaction_data->school_name ?></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Amount </td>
                                                <td> <strong style="color:crimson"> <?php echo $currency_symbol ?> <?php echo $transaction_data->transaction_amount ?> </strong></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Date</td>
                                                <td> <?php echo $transaction_data->transaction_date ?></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Purpose</td>
                                                <td> <?php echo $transaction_data->transaction_purpose ?></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Recepient</td>
                                                <td> <?php echo $transaction_data->transaction_recepient ?></td>
                                            </tr>
                                        </table>
                                    </center>
                                </div>
                                <button class="btn btn-primary" onclick="PrintDiv()"> <span class="fa fa-print"></span> Print</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--body wrapper end-->

