

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Debit Wallet
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Debit Wallet</a>
                    </li>
                    <li class="active"> <a href="<?php echo site_url('Administrator/Students/view') ?>"></a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Debit Student's Wallet
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'success') : '' ?>
                                        <?php echo form_open('');?>
                                        <div class="form-group">
                                            <label class="col-md-2">Student Name</label>
                                            <div class="col-md-3">
                                                <?php
                                                    $students_array = array();
                                                    $students_array[] = 'Select Student';
                                                    if(count($students)){
                                                        foreach ($students as $student):
                                                            $students_array[$student->uniqueid] = $student->surname.' '.$student->othernames;
                                                        endforeach;
                                                    }
                                                    echo form_dropdown('uniqueid',$options = $students_array,  set_value('studentid'),'class = "form-control" required = "required"');
                                                ?>
                                            </div>
                                            <label class="col-md-2">or Account Number</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" name = "account_number">
                                            </div>
                                            <button class="btn btn-primary"> <span class="fa fa-search"> </span> Proceed</button>
                                        </div>
                                    </div>
                                   <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--body wrapper end-->

