

<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Transaction Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Transaction History</a>
                    </li>
                    <li class="active"> <a href="#existing_users">  </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a id ="existing_users"></a>
                            Transaction History for <?php echo $transactions[0]->surname.' '.$transactions[0]->othernames; ?>
                        </div>
                        <?php
                        if (count($transactions)) {
                            ?>
                            <div class="panel-body" id = "printArea">
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <div class="table-responsive">
                                        <table class="display table table-bordered table-striped" border = "1" width = "100%" style="border-collapse: collapse">
                                            <tr>
                                                <td colspan="3">   <center> <img src="<?php echo getResource('images/login_logo.png') ?>" style="max-width:200px" alt=""/> </center></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">   <center> <h4> Credit Transactions History for <?php echo $transactions[0]->surname . ' ' . $transactions[0]->othernames; ?></h4> </center></td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Student Name
                                                </td>
                                                <td>
                                                    <?php echo $transactions[0]->surname . ' ' . $transactions[0]->othernames; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Account Number
                                                </td>
                                                <td>
                                                    <?php echo $transactions[0]->uniqueid ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    School Name
                                                </td>
                                                <td>
                                                    <?php echo $transactions[0]->school_name; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    <table  class="display table table-bordered table-striped" border = "1" width = "100%" style="border-collapse: collapse">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th> Beneficiary </th>
                                                <th> School Name </th>
                                                <th> Transaction Amount </th>
                                                <th> Transaction Date </th>
                                                <th> Transaction Made By </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            $total = 0;
                                            foreach ($transactions as $value):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>
                                                    <td> <?php echo $value->surname.' '.$value->othernames; ?> </td>
                                                    <td> <?php echo $value->school_name; ?> </td>
                                                    <td> <?php echo $currency_symbol.' '.number_format($value->transaction_amount,2); ?> </td>
                                                    <td> <?php echo $value->transaction_date; ?> </td>
                                                    <td> <?php echo $value->transaction_made_by; ?> </td>
                                                </tr>
                                                <?php
                                                $total = $total + $value->transaction_amount;
                                                $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                                <tr class="">
                                                    <td> </td>
                                                    <td>  </td>
                                                    <td> Total</td>
                                                    <td> <?php echo $currency_symbol.' '.number_format($total,2); ?> </td>
                                                    <td>  </td>
                                                    <td>  </td>
                                                </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        <button class="btn btn-primary" onclick="PrintDiv()"> <span class="fa fa-print"></span> Print</button>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->

