

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Fund Wallet
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Fund Wallet</a>
                    </li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Edit Student Transaction
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12">
                                            <?php if(isset($student_data)): ?>
                                                <table class="table table-bordered" width = "80%" >
                                                    <tr>
                                                        <td>Account Number</td>
                                                        <td> <strong style="color:crimson"> <?php echo $student_data->uniqueid ?> </strong></td>
                                                        <td rowspan="6"> <img src="<?php echo base_url($student_data->passport) ?>" width="100px"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surname</td>
                                                        <td> <?php echo strtoupper($string = $student_data->surname) ?></td>

                                                    </tr>
                                                    <tr>
                                                        <td>Othernames</td>
                                                        <td> <?php echo strtoupper($string = $student_data->othernames) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gender</td>
                                                        <td> <?php echo $student_data->gender ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Parent Phone Number</td>
                                                        <td> <?php echo $student_data->parent_phone_number ?></td>
                                                    </tr>
                                                </table>
                            <?php echo form_open('') ?>
                                <div class="col-lg-12">
                                        <h1> Edit <?php echo strtoupper($string = $student_data->surname).' '.strtoupper($string = $student_data->othernames) ?>'s Transaction </h1>
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <div class="form-group">
                                            <label title="How much money was paid on behalf of the student?" data-toggle="tooltip">Amount</label>
                                            <input class="form-control" name = "transaction_amount" type = "text" required ="" placeholder="Transaction Amount" value = "<?php echo set_value('transaction_amount',  str_replace('.00','',$transaction_data->transaction_amount)) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label title="Which date was the money paid at the bank?" data-toggle="tooltip"> Transaction Date</label>
                                            <input class="form-control" name = "transaction_date" type = "date" required ="" placeholder="Transaction Date" value = "<?php echo set_value('transaction_date',$transaction_data->transaction_date) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label title="Who paid in the money?" data-toggle="tooltip"> Payment Made By</label>
                                            <input class="form-control" name = "transaction_made_by" type = "text"  placeholder="Transaction Made By" value = "<?php echo set_value('transaction_made_by', $transaction_data->transaction_made_by) ?>">
                                        </div>
                                    <button type="submit" class="btn btn-primary"> Save</button>
                                </div>

                                            <?php endif;?>
                                        </div>
                                    </div>
                                   <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--body wrapper end-->

