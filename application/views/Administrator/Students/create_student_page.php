s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Student Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Manage Student</a>
                    </li>
                    <li class="active"> <a href="<?php echo site_url('Administrator/Students/view') ?>"> View Existing Students </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                        <?php echo isset($message)?$message:''?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo!empty($student->surname) ? '<i class = "fa fa-edit"></i> Editing Student : ' . $student->surname : '<i class = "fa fa-plus"></i> Add New Student' ?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                   <?php echo form_open_multipart('') ?>
                                    <div class="col-lg-6">
                                        <h4> Basic Information </h4>
                                            <div class="col-md-12">
                                                <label>Student Passport</label>
                                                <i> Only.Jpeg format allowed : Maximum Allowed Size : 100kb</i>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-preview thumbnail col-md-6" style="max-width:150px; border:0px;"><img width="100px" src="<?php echo!empty($student->passport) ? base_url($student->passport) : "" ?>" /></div>
                                                    <div class="col-md-6" >
                                                        <span class="btn btn-success col-lg-8">
                                                            <input type="file"  name="passport"  />
                                                        </span>
                                                        <a href="#" class="btn btn-danger fileupload-exists pull-right col-lg-4" data-dismiss="fileupload">Remove</a>
                                                    </div>

                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <label>Surname</label>
                                            <input class="form-control" name = "surname" type = "text" required ="" placeholder="Surname" value = "<?php echo set_value('surname', $student->surname) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Othernames</label>
                                            <input class="form-control" name = "othernames" type = "text" required ="" placeholder="Othernames" value = "<?php echo set_value('othernames', $student->othernames) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <?php
                                                $gender_options = array(''=>'Select Gender','Male'=>'Male','Female'=>'Female');
                                                echo form_dropdown('gender',$gender_options,  set_value('gender',$student->gender),'class = "form-control" required = "required"');
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <input class="form-control" name = "date_of_birth" type = "date" placeholder="Date of Birth" value = "<?php echo set_value('date_of_birth', $student->date_of_birth) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>School Name</label>
                                            <?php
                                                if (count($schools)){
                                                    $school_options = array();
                                                    $school_options[] = 'Select School';
                                                    foreach ($schools as $school):
                                                        $school_options[$school->uniqueid] = $school->school_name;
                                                    endforeach;
                                                    echo form_dropdown('school_id',$school_options, set_value('school_id',$student->school_id),'class = "form-control" required = "required"');
                                                }
                                                else{
                                            ?>
                                                    <div class="alert alert-error"> No School has been enrolled yet, Enrol a School to Proceed </div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                </div>
                                <div class="col-lg-6">
                                        <h4> Parent Information </h4>
                                        <div class="form-group">
                                            <label>Parent Name</label>
                                            <input class="form-control" name = "parent_name" type = "text" required ="" placeholder="Parent Name" value = "<?php echo set_value('surname', $student->surname) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Parent Phone Number</label>
                                            <input class="form-control" name = "parent_phone_number" type = "text" maxlength="11" required ="" placeholder="Parent Phone Number" value = "<?php echo set_value('parent_phone_number', $student->parent_phone_number) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Parent Email Address</label>
                                            <input class="form-control" name = "parent_email_address" type = "email"  placeholder="Parent Email Address" value = "<?php echo set_value('parent_email_address', $student->parent_email_address) ?>">
                                        </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php
                                        $options = array('1' => 'Enabled',
                                            '2' => 'Disabled');
                                        echo form_dropdown('status', $options, set_value('status', $student->status), 'class = "form-control"')
                                        ?>
                                    </div>

                                    <button type="submit" class="btn btn-primary"><?php echo!empty($student->surname) ? '<i class = "fa fa-edit"></i> Update Student' : '<i class = "fa fa-plus"></i> Add Student'; ?></button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>


<?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--body wrapper end-->

