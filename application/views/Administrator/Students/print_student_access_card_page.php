s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Student Access Card
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Student Information
                            </div>
                            <div class="panel-body">
                                <div class="row" id = "printArea">
                                    <center>
                                        <table class="" width = "600px" border = "1" style="border: #000 2px; border-collapse: collapse" >
                                            <tr>
                                                <td colspan="3">   <center> <img src="<?php echo getResource('images/login_logo.png') ?>" style="max-width:200px" alt=""/> </center></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">  <center> <h4> Student Access Card </center></td>
                                            </tr>
                                            <tr>
                                                <td>Account Number</td>
                                                <td> <strong style="color:crimson"> <?php echo $student->uniqueid ?> </strong></td>
                                                <td rowspan="6"> <img src="<?php echo base_url($student->passport) ?>" width="100px"/></td>
                                            </tr>
                                            <tr>
                                                <td>Surname</td>
                                                <td> <?php echo strtoupper($string = $student->surname) ?></td>

                                            </tr>
                                            <tr>
                                                <td>Othernames</td>
                                                <td> <?php echo strtoupper($string = $student->othernames) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td> <?php echo $student->gender ?></td>
                                            </tr>
                                            <tr>
                                                <td>School Name</td>
                                                <td> <?php echo $student->school_name ?></td>

                                            </tr>
                                            <tr>
                                                <td>Parent Phone Number</td>
                                                <td> <?php echo $student->parent_phone_number ?></td>

                                            </tr>
                                        </table>
                                    </center>
                                </div>
                                <button class="btn btn-primary" onclick="PrintDiv()"> <span class="fa fa-print"></span> Print</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--body wrapper end-->

