s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Student Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Manage Student</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> View Existing Students </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a id ="existing_users"></a>
                            Existing Students
                        </div>

                        <?php
                        if (count($students)) {
                            ?>
                            <div class="panel-body">
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                <div class="table-responsive">

                                    <a href = "<?php echo site_url('Administrator/Students') ?>#add_users" > <button class ="btn btn-primary" style = "margin-bottom:10px"> <i class ="fa fa-plus"></i> Add New Student </button> </a>

                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Account Number</th>
                                                <th>Surname</th>
                                                <th>Othernames</th>
                                                <th>School Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            foreach ($students as $studentdata):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>
                                                    <td> <?php echo ucwords($studentdata->uniqueid); ?> </td>
                                                    <td> <?php echo ucwords($studentdata->surname); ?> </td>
                                                    <td> <?php echo ucwords($studentdata->othernames); ?> </td>
                                                    <td> <?php echo ucwords($studentdata->school_name); ?> </td>
                                                    <td> <?php echo $studentdata->status == 1? '<span class="label label-success"> Enabled </span>' : '<span class="label label-danger"> Disabled </span>'; ?> </td>
                                                    <td>
                                                        <div class="dropdown">
                                                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                          <span class="caret"></span></button>
                                                          <ul class="dropdown-menu">
                                                              <li> <a href="<?php echo site_url('Administrator/Students/view_student/'.$studentdata->id) ?>" target="_blank"> View Student</a></li>
                                                                <li> <a href="<?php echo site_url('Administrator/Students/index/'.$studentdata->id) ?>"> Edit Student</a></li>
                                                                <li> <a onclick="return(confirm('Are you sure you want to delete? \n It cannot be undone o'))" href="<?php echo site_url('Administrator/Students/delete/'.$studentdata->id) ?>"> Delete Student</a></li>
                                                                <li><a href="<?php echo site_url('Administrator/Students/print_access_card/'.$studentdata->id.'/'.$studentdata->uniqueid) ?>" target="_blank"> Print Access Card</a></li>
                                                          </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <?php
                        }
                        else {
                            echo getAlertMessage('Sorry : You dont have any registered users yet');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->

