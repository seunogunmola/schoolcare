

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Enquiries
                </h3>
                <a id = "add_Resources"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">View Enquiries</a>
                    </li>

                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">


                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Enquiries
                            </div>
                            <div class="panel-body">
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Sender Name</th>
                                                <th>Email</th>
                                                <th> Phone Number </th>
                                                <th> Message </th>
                                                <th>Date Sent</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php $sn = 1;
                                            foreach ($enquiries as $enquiries_data):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>
                                                    <td> <?php echo $enquiries_data->sender_name; ?> </td>
                                                    <td> <?php echo $enquiries_data->sender_email; ?> </td>
                                                    <td> <?php echo $enquiries_data->sender_phone_number; ?> </td>
                                                    <td> <?php echo limit_to_words($enquiries_data->message,10); ?> </td>
                                                    <td> <?php echo $enquiries_data->date_sent; ?> </td>
                                                    <td> <?php echo $enquiries_data->status == 0?'<i class = "fa fa-envelope alert-danger"></i>':'<div class="label label-info">Read</div>'; ?> </td>
                                                    <td>
                                                        <a href="<?php echo site_url('Administrator/enquiries/read/' . $enquiries_data->id) ?>" target="_blank"> <i class="fa fa-search" title="View Enquiry"></i></a>

                                                        <?php echo getBtn('enquiries/delete/' . $enquiries_data->id, 'delete') ?>
                                                    </td>
                                                </tr>
                                                <?php $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--body wrapper end-->

