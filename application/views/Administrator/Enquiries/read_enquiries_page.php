<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Enquiries
                </h3>
                <a id = "add_Resources"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">View Enquiries</a>
                    </li>

                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">


                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Enquiry from : <?php echo $enquiry->sender_name; ?>
                            </div>
                            <div class="panel-body">
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                            <tr>
                                                <td> Sender Name </td>
                                                <td> <?php echo $enquiry->sender_name; ?> </td>
                                            </tr>
                                            <tr>
                                                <td>Sender Email </td>
                                                <td> <?php echo $enquiry->sender_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Sender Phone Number </td>
                                                <td> <?php echo $enquiry->sender_phone_number; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Message</td>
                                                <td><?php echo $enquiry->message; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Date Sent</td>
                                                <td><?php echo $enquiry->date_sent; ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td> <a href="<?php echo site_url('Administrator/Enquiries/MarkReadMessage/'.$enquiry->id)?>"> <button class="btn btn-info btn-small"> <i class="fa fa-check"></i> Mark as Read</button> </a></td>
                                            </tr>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--body wrapper end-->

