s

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Agent Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Manage Agents</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> View All Agents </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo!empty($agent->username) ? '<i class = "fa fa-edit"></i> Editing Agent : ' . $agent->username : '<i class = "fa fa-plus"></i> Add New Agent' ?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $message ?>
                                    <?php echo form_open('') ?>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control" name = "username" type = "text" required ="" placeholder="username" value = "<?php echo set_value('username', $agent->username) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" name = "password" placeholder="Enter text" type = "password" <?php echo empty($agent->username) ? 'required = ""' : '' ?>>
                                        </div>

                                        <div class="form-group">
                                            <label>Fullname</label>
                                            <input class="form-control"  name = "fullname" type = "text" placeholder="Enter Agent's Fullname" value = "<?php echo set_value('fullname', $agent->fullname) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Assigned School</label>
                                            <?php
                                                if (count($schools)){
                                                    $school_options = array();
                                                    $school_options[''] = 'Select School';
                                                    foreach ($schools as $school):
                                                        $school_options[$school->uniqueid] = $school->school_name;
                                                    endforeach;
                                                    echo form_dropdown('school_id',$school_options, set_value('school_id',$agent->school_id),'class = "form-control" required = "required"');
                                                }
                                                else{
                                            ?>
                                                    <div class="alert alert-error"> No School has been enrolled yet, Enrol a School to Proceed </div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="slide-toggle">
                                                <div class="col-md-12">
                                                    <?php
                                                    $privilege = array();
                                                    if (isset($agent->privileges)):
                                                        if ($agent->privileges == 'all') {
                                                            $privilege[] = $agent->privileges;
                                                        }
                                                        else {
                                                            $privilege = explode('-', $agent->privileges);
                                                        }
                                                    endif;

                                                    ?>
                                                    <input type="checkbox" name ="all" id="selecctall" class="js-switch-red" onchange="check_all()"  <?php echo in_array('all', $privilege) ? 'checked' : '' ?>   />
                                                    <label>All</label

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <br/>
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <input type="checkbox"  name = "privileges[]" value ="credit_wallet" class="js-switch" onclick=""  <?php echo in_array('credit_wallet', $privilege) || in_array('all', $privilege) ? 'checked' : '' ?> />
                                                        <label>Credit Wallet</label>
                                                    </div>


                                                    <div class="col-md-3">
                                                        <input type="checkbox" name = "privileges[]" value ="debit_wallet" class="js-switch-blue" <?php echo in_array('debit_wallet', $privilege) || in_array('all', $privilege) ? 'checked' : '' ?>  />
                                                        <label>Debit Wallet</label>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <input type="checkbox" name = "privileges[]" value ="reporting" class="js-switch-pink" <?php echo in_array('reporting', $privilege) || in_array('all', $privilege) ? 'checked' : '' ?>/>
                                                        <label>Reporting</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input class="form-control" name = "email_address" required="" type = "text" placeholder="Enter Agent's email Address" value = "<?php echo set_value('email_address', $agent->email_address) ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input class="form-control" name = "phone_number" type = "text" placeholder="Enter Agent's Phone Number" required="" value = "<?php echo set_value('phone_number', $agent->phone_number) ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php
                                        $options = array('1' => 'Enabled',
                                            '2' => 'Disabled');
                                        echo form_dropdown('status', $options, set_value('status', $agent->status), 'class = "form-control"')
                                        ?>
                                    </div>

                                    <button type="submit" class="btn btn-primary"><?php echo!empty($agent->username) ? '<i class = "fa fa-edit"></i> Update Agent' : '<i class = "fa fa-plus"></i> Add Agent'; ?></button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>


<?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a id ="existing_users"></a>
                            Existing Agents
                        </div>
                        <?php
                        if (count($agents)) {
                            ?>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <a href = "<?php echo site_url('admin/users') ?>#add_users" > <button class ="btn btn-primary" style = "margin-bottom:10px"> <i class ="fa fa-plus"></i> Add New Agent </button> </a>

                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Username</th>
                                                <th> Fullname</th>
                                                <th>Email Address</th>
                                                <th>Phone Number</th>
                                                <th>School Assigned</th>
                                                <th>Access Level</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            foreach ($agents as $agentdata):
                                                ?>
                                                <tr class="">
                                                    <td> <?php echo $sn; ?> </td>
                                                    <td> <?php echo $agentdata->username; ?> </td>
                                                    <td> <?php echo ucwords($agentdata->fullname); ?> </td>
                                                    <td> <?php echo $agentdata->email_address; ?> </td>
                                                    <td> <?php echo $agentdata->phone_number; ?> </td>
                                                    <td> <?php echo $agentdata->school_name; ?> </td>
                                                    <td> <?php echo $agentdata->privileges; ?> </td>
                                                    <td> <?php echo $agentdata->status == 1? '<span class="label label-success"> Enabled </span>' : '<span class="label label-danger"> Disabled </span>'; ?> </td>
                                                    <td>
                                                        <?php echo getBtn('Agents/view/' . $agentdata->id, 'view') ?>
        <?php echo getBtn('Agents/index/' . $agentdata->id, 'edit') ?>
                                                <?php echo getBtn('Agents/delete/' . $agentdata->id, 'delete') ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                $sn++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <?php
                        }
                        else {
                            echo getAlertMessage('Sorry : You dont have any registered agents yet');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->

