
<!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="<?php echo site_url('Agent/dashboard') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <?php if(isset($agent_privileges) && in_array('all', $agent_privileges)): ?>
                    <li class="menu-list"><a href=""><i class="fa fa-money"></i> <span>Debit Wallet</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="<?php echo site_url('Agent/Outgoing_transactions')?>"> Debit Wallet</a></li>
                            <li><a href="<?php echo site_url('Agent/Outgoing_transactions/History')?>"> Debit Transactions History</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if(isset($agent_privileges) && in_array('all', $agent_privileges)): ?>
                        <li class="menu-list"><a href=""><i class="fa fa-bar-chart"></i> <span>Reports</span></a>
                            <ul class="sub-menu-list">
                                <li><a href="<?php echo site_url('Agent/Reports/Credit_transactions')?>"> Credit Transactions Report</a></li>
                                <li><a href="<?php echo site_url('Agent/Reports/Debit_transactions')?>"> Debit Transactions Report</a></li>
                            </ul>
                        </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>