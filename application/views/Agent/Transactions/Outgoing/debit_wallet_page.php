

<body class="sticky-header">

    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Debit Wallet
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Debit Wallet</a>
                    </li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Debit Student's Wallet
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12">
                                            <?php if(isset($student_data)): ?>
                                        <a href="<?php echo site_url('Agent/Outgoing_transactions') ?>"> <div style="color:crimson" class="pull-right" title="I choose the wrong student" data-toggle = "tooltip"> <h3> Not this Students? Go Back </h3> </div> </a>
                                                <table class="table table-bordered" width = "80%" >
                                                    <tr>
                                                        <td>Account Number</td>
                                                        <td> <strong style="color:crimson"> <?php echo $student_data->uniqueid ?> </strong></td>
                                                        <td rowspan="6"> <img src="<?php echo base_url($student_data->passport) ?>" width="100px"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surname</td>
                                                        <td> <?php echo strtoupper($string = $student_data->surname) ?></td>

                                                    </tr>
                                                    <tr>
                                                        <td>Othernames</td>
                                                        <td> <?php echo strtoupper($string = $student_data->othernames) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gender</td>
                                                        <td> <?php echo $student_data->gender ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Parent Phone Number</td>
                                                        <td> <?php echo $student_data->parent_phone_number ?></td>
                                                    </tr>
                                                </table>

                                        <div align = "center">
                                            <h1 style="color:crimson">
                                                Account Balance : <?php echo $currency_symbol.' '.number_format($account_balance,2) ?>
                                            </h1>
                                        </div>
                            <?php echo form_open('') ?>
                                <div class="col-lg-12">
                                        <h1> Debit <?php echo strtoupper($string = $student_data->surname).' '.strtoupper($string = $student_data->othernames) ?>'s Wallet </h1>

                                        <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <div class="form-group">
                                            <label title="How much money was paid on behalf of the student?" data-toggle="tooltip">Amount</label>
                                            <input class="form-control" name = "transaction_amount" type = "text" required ="" placeholder="Transaction Amount" value = "<?php echo set_value('transaction_amount') ?>">
                                        </div>
                                        <div class="form-group">
                                            <label title="Which date was the money paid at the bank?" data-toggle="tooltip"> Transaction Date</label>
                                            <input class="form-control" name = "transaction_date" type = "date" required ="" placeholder="Transaction Date" value = "<?php echo set_value('transaction_date',date('Y-m-d')) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label title="Who paid in the money?" data-toggle="tooltip"> Transaction Purpose</label>
                                            <textarea class="form-control" name = "transaction_purpose"><?php echo set_value('transaction_purpose')?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label title="Who is this money paid to?" data-toggle="tooltip"> Transaction Recepient</label>
                                            <input class="form-control" name = "transaction_recepient" type = "text" required ="" placeholder="Transaction Recepient" value = "<?php echo set_value('transaction_recepient') ?>">
                                        </div>
                                    <button type="submit" class="btn btn-primary"> Debit Wallet</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>

                                            <?php endif;?>
                                        </div>
                                    </div>
                                   <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--body wrapper end-->

