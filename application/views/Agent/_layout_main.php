<?php
#LOAD HEADER
$this->load->view('Agent/_templates/_head');

#LOAD MENU
$this->load->view('Agent/_templates/_menu');

#LOAD SUBVIEW
$this->load->view($subview);

#LOAD FOOTER
$this->load->view('Agent/_templates/_foot');