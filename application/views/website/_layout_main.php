<?php
#LOAD HEADER
$this->load->view('website/_templates/_head');

#LOAD MENU
//$this->load->view('website/_templates/_menu');


#LOAD EXTRA VIEWS, You can cantenate more views to appear 
	if(isset($extraview)):
		foreach($extraview as $value):
		  $this->load->view($value);
		endforeach;
	endif;

#LOAD SUBVIEW
$this->load->view($subview);

#LOAD FOOTER
$this->load->view('website/_templates/_foot');