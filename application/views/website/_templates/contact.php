<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
<!-- contact -->
	<div id="contact" class="contact">
		<div class="container">
                        <div class="col-md-12" style="margin-bottom:20px;margin-top:6em;">
                            <center>  <h1>Contact Us</h1> </center>
                        </div>
                        <div class="col-md-4 contact-left" style="margin-top:1em;">
				<h2>Address</h4>
                                <h4>SchoolCare Nigeria </h4>
                                <ul style="margin-top: -1em">
					<li>Phone :080 SCHOOLCARE</li>
					<li><a href="mailto:info@example.com">schoolcare.com</a></li>
				</ul>
			</div>
			<div class="col-md-8 contact-left">
				<h4>Contact Form</h4>
                                <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <form action = "" method="post">
                                    <input type="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="" name="sender_name">
                                    <input type="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="" name="sender_email">
                                    <input type="text" value="Telephone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Telephone';}" required="" name="sender_phone_number">
                                    <textarea name="message" type="text"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="" placeholder="Message"></textarea>
                                    <div class="g-recaptcha" data-sitekey="6LdbfiITAAAAAPCSXp5QBpFC5rC34VAf9viblkSB"></div>     
                                    <br/>
                                    <button type="submit" class ="btn btn-primary"> <span class="glyphicon glyphicon-send"></span> Submit </button>
                                    <button type="reset" class ="btn btn-danger"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //contact -->