<!DOCTYPE html>
<html>
    <head>
        <title>School Care</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Attention Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); }
        </script>

        <!-- //for-mobile-apps -->
        <link href="<?php echo getResource('website/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo getResource('website/css/style.css'); ?>" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="<?php echo getResource('website/js/jquery-1.11.1.min.js'); ?>"></script>
        <!-- //js -->
        <!--script-->
        <script src="<?php echo getResource('website/js/jquery.chocolat.js'); ?>"></script>
        <link rel="stylesheet" href="<?php echo getResource('website/css/chocolat.css'); ?>" type="text/css" media="screen" charset="utf-8">



        <!--light-box-files-->
        <script type="text/javascript" charset="utf-8">
$(function () {
    $('.gallery-grid a').Chocolat();
});
        </script>
        <!--script-->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="<?php echo getResource('website/js/move-top.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo getResource('website/js/easing.js'); ?>"></script>
        <script type="text/javascript">
jQuery(document).ready(function ($) {
    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
    });
});
        </script>
        <!-- start-smoth-scrolling -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        <style type="text/css">
            body { 
                padding-top: 65px; 
            }
        </style>
    </head>

    <body>
        <!-- header -->
        <div class="header-top navbar-fixed-top">
            <div class="container">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="header-top-left">
                            <a href="index.html">
                                <img src="<?php echo getResource('images/login_logo.png'); ?>" style="width:250px; height:70px;"/>
                            </a>
                            <!--<p style="font-family:Questrial; color:#fff;"><b>We bring are bringing your child in school many steps closer to you</b></p> -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="header-nav ">
                            <div class="container">
                                <div class="header-nav-bottom">
                                    <nav class="navbar navbar-default ">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="<?= site_url('website/home'); ?>">Home</a></li>
                                                <li><a href="<?= site_url('website/shop'); ?>" >Shop</a></li>
                                                <li><a href="<?= site_url('website/about'); ?>" >About</a></li>
                                                <li><a href="<?= site_url('website/contact'); ?>" >Contact Us</a></li>
                                            </ul>
                                        </div><!-- /.navbar-collapse -->
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="header-top-right text-right">

                        <?php if (!$this->session->loggedin) { ?>
                            <div class="more pull-right">
                                <a href="<?= site_url('website/authenticate'); ?>" class="hvr-shadow-radial" style="color:#fff;">Login</a>
                            </div>

                            <div class="more">
                                <a href="<?= site_url('website/enroll'); ?>" class="hvr-shadow-radial" style="color:#fff;">Enroll</a>
                            </div>
    <?php
}
else {
    ?>

                            <div class="more pull-right">
                                <a href="<?= site_url('website/logout'); ?>" class="hvr-shadow-radial" style="color:#fff;">Logout</a>
                            </div>

                            <div class="more">
                                <a href="<?= site_url('website/dashboard'); ?>" class="hvr-shadow-radial" style="color:#fff;">Dashboard</a>
                            </div>

    <?php
}
?>
                    </div>
                </div>
                <div class="clearfix"> </div>

            </div>
        </div>



        <!-- //header -->
        <!-- banner -->

