
	<div class="banner">
		<div class="container"> 
			<div class="banner-info">
				<h1>Change is the end result<span> of all true learning.</span></h1>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
					eiusmod tempor.</p>
				<ul class="social-icons">
					<li><a href="#" class="facebook"></a></li>
					<li><a href="#" class="twitter"></a></li>
					<li><a href="#" class="be"></a></li>
					<li><a href="#" class="p"></a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- //banner -->