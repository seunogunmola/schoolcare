<!-- gallery -->
	<div id="gallery" class="gallery">
		<div class="container">
			<h3>Photo Gallery</h3>
			<div class="gallery-grids">
				<div class="gallery-grid">
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/5.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/5.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4>Enimet pulvinar posuere</h4>
									<p>In sit amet sapien eros Integer dolore magna aliqua</p>	
								</figcaption>
							</a>
						</figure>
					</div>
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/9.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/9.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4 class="hjk">Enimet pulvinar</h4>
									<p>In sit amet sapien eros</p>	
								</figcaption>
							</a>
						</figure>
					</div>
				</div>
				<div class="gallery-grid">
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/10.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/10.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4 class="hjk">Enimet pulvinar</h4>
									<p>In sit amet sapien eros</p>	
								</figcaption>
							</a>
						</figure>
					</div>
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/6.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/6.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4>Enimet pulvinar posuere</h4>
									<p>In sit amet sapien eros Integer dolore magna aliqua</p>	
								</figcaption>
							</a>
						</figure>
					</div>
				</div>
				<div class="gallery-grid">
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/7.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/7.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4>Enimet pulvinar posuere</h4>
									<p>In sit amet sapien eros Integer dolore magna aliqua</p>	
								</figcaption>
							</a>
						</figure>
					</div>
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/11.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/11.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4 class="hjk">Enimet pulvinar</h4>
									<p>In sit amet sapien eros</p>	
								</figcaption>
							</a>
						</figure>
					</div>
				</div>
				<div class="gallery-grid">
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/12.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/12.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4 class="hjk">Enimet pulvinar</h4>
									<p>In sit amet sapien eros</p>	
								</figcaption>
							</a>
						</figure>
					</div>
					<div class="gallery-grid1">
						<figure class="effect-bubba">
							<a href="images/8.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
								<img src="images/8.jpg" alt=" " class="img-responsive" />
								<figcaption>
									<h4>Enimet pulvinar posuere</h4>
									<p>In sit amet sapien eros Integer dolore magna aliqua</p>	
								</figcaption>
							</a>
						</figure>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //gallery -->