
<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="about-grids">
				<div class="col-md-6 about-grid-left">
					<img src="<?php echo getResource('website/images/hostel1.jpg'); ?>" alt=" " class="img-responsive img-rounded" />
				</div>
				<div class="col-md-6 about-grid-right">
					<h3> You can call us parent in school</h3>
					<p>We bring are bringing your child in school many steps closer to you.</p>
					<div class="more">
						<a href="<?= site_url(''); ?>" class="hvr-shadow-radial">Learn More</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //about -->