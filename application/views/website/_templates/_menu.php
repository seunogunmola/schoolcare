
<!-- header-nav -->
	<div class="header-nav ">
		<div class="container">
			<div class="header-nav-bottom">
				<nav class="navbar navbar-default ">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					 <ul class="nav navbar-nav">
						<li class="active"><a href="<?= site_url('website/home'); ?>">Home</a></li>
						<li><a href="<?= site_url('website/shop'); ?>" >Shop</a></li>
					    <li><a href="<?= site_url('website/about'); ?>" >About</a></li>
						<li><a href="<?= site_url('website/contact'); ?>" >Contact Us</a></li>
					  </ul>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>
<!-- //header-nav -->