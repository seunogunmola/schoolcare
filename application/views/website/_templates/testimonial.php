
<!-- testimonials -->
	<div class="testimonials" style="margin-top:50px;">
		<div class="container">
			<h3>Testimonials</h3>
			<div class="testimonials-grids">
				<ul id="flexiselDemo1">			
					<li>
						<div class="testimonials-grid">
							<div class="col-xs-5 testimonials-grid-left">
								<img src="<?php echo getResource('website/images/1.png'); ?>" alt=" " class="img-responsive" />
							</div>
							<div class="col-xs-7 testimonials-grid-right">
								<div class="rating">
									<span style="color:#0066ff;">☆</span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
								</div>
								<p>Itaque earum rerum hic tenetur a sapiente delectus.<span>Laura</span></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</li>
					<li>
						<div class="testimonials-grid">
							<div class="col-xs-5 testimonials-grid-left">
								<img src="<?php echo getResource('website/images/3.png'); ?>" alt=" " class="img-responsive" />
							</div>
							<div class="col-xs-7 testimonials-grid-right">
								<div class="rating">
									<span style="color:#0066ff;">☆</span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
								</div>
								<p>Itaque earum rerum hic tenetur a sapiente delectus.<span>James</span></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</li>
					<li>
						<div class="testimonials-grid">
							<div class="col-xs-5 testimonials-grid-left">
								<img src="<?php echo getResource('website/images/2.png'); ?>" alt=" " class="img-responsive" />
							</div>
							<div class="col-xs-7 testimonials-grid-right">
								<div class="rating">
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
									<span style="color:#0066ff;"><b>☆</b></span>
								</div>
								<p>Itaque earum rerum hic tenetur a sapiente delectus.<span>williumson</span></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</li>
				</ul>
					<script type="text/javascript">
							$(window).load(function() {
								$("#flexiselDemo1").flexisel({
									visibleItems: 3,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 1
										}, 
										landscape: { 
											changePoint:640,
											visibleItems:2
										},
										tablet: { 
											changePoint:768,
											visibleItems: 2
										}
									}
								});
								
							});
					</script>
					<script type="text/javascript" src="<?php echo getResource('website/js/jquery.flexisel.js'); ?>"></script>
			</div>
		</div>
	</div>
<!-- //testimonials -->