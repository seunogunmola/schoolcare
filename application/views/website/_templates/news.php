<!-- news -->
	<div class="news" id="news">
		<div class="container">
			<h3>News & Events</h3>
			<div class="latest-news">
				<div class="latest-news-grids">
					<div class="col-md-6 latest-news-grid">
						<div class="col-xs-4 latest-news-grid-left">
							<img src="images/13.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-xs-8 latest-news-grid-right">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015</span></p>
							<h4>fugiat quo voluptas nulla pariatur</h4>
							<p class="man">But who has any right to find fault with a man who 
							chooses to enjoy.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 latest-news-grid">
						<div class="col-xs-4 latest-news-grid-left">
							<img src="images/14.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-xs-8 latest-news-grid-right">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015</span></p>
							<h4>fugiat quo voluptas nulla pariatur</h4>
							<p class="man">But who has any right to find fault with a man who 
							chooses to enjoy.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="latest-news-grids">
					<div class="col-md-6 latest-news-grid">
						<div class="col-xs-4 latest-news-grid-left">
							<img src="images/15.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-xs-8 latest-news-grid-right">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015</span></p>
							<h4>fugiat quo voluptas nulla pariatur</h4>
							<p class="man">But who has any right to find fault with a man who 
							chooses to enjoy.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 latest-news-grid">
						<div class="col-xs-4 latest-news-grid-left">
							<img src="images/16.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-xs-8 latest-news-grid-right">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015</span></p>
							<h4>fugiat quo voluptas nulla pariatur</h4>
							<p class="man">But who has any right to find fault with a man who 
							chooses to enjoy.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="news-grid">
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left1">
						<div class="news-grid-left-top">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015 at 10:30 AM</span></p>
						</div>
						<h4>perferendis doloribus asperiores</h4>
						<p class="vel">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur.</p>
						<div class="news-pos">
							<p>Vel illum qui dolorem eum 
								fugiat quo voluptas nulla pariatur</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left2">
						<div class="news-grid-left-top">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015 at 10:30 AM</span></p>
						</div>
						<h4>perferendis doloribus asperiores</h4>
						<p class="vel">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur.</p>
						<div class="news-pos">
							<p>Vel illum qui dolorem eum 
								fugiat quo voluptas nulla pariatur</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left3">
						<div class="news-grid-left-top">
							<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>September 25th 2015 at 10:30 AM</span></p>
						</div>
						<h4>perferendis doloribus asperiores</h4>
						<p class="vel">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur.</p>
						<div class="news-pos">
							<p>Vel illum qui dolorem eum 
								fugiat quo voluptas nulla pariatur</p>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="newsletter">
				<form>
					<input type="text" value="Enter Your Mail..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Mail...';}" required="">
					<input type="submit" value="Subscribe" >
				</form>
			</div>
		</div>
	</div>
<!-- //news -->