<!-- banner-bottom -->
<div class="banner-bottom">
    <div class="container">
        <div class="banner-bottom-grids">
            <div class="col-md-6 banner-bottom-grid-left text-center">
                <div class="col-xs-12">
                    <h3>How School Care Works</h3>
                </div>
                <div class="banner-bottom-grid-left-grids">
                    <div class="col-md-12 banner-bottom-grid-left-grdl">

                        <center>
                            <div class="banner-bottom-grid-fig">
                                <span class="glyphicon glyphicon-education" aria-hidden="true"></span>
                            </div>
                        </center>

                        <p>
                            School Care is a Fund Management Platform for students in the Boarding house. We eliminate money mismanagement, loss and we also take away your worries about what your child in the boarding house use money for. <br/><br/>
                        </p>

                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="more">
                    <a href="#" class="hvr-shadow-radial">For More Info...</a>
                </div>
            </div>
            <div class="col-md-6 banner-bottom-grid-right">
                <div class="banner-bottom-grid-right1 text-center" style="background-color:#ff6600">
                    <div class="col-xs-12 text-center" style="margin-bottom:10px;"> <h3> Step 1 </h3> </div>
                    <div class="col-xs-4 banner-bottom-grid-right1-left text-center">
                        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-8 banner-bottom-grid-right1-right text-left">
                        <h4> Get your child enrolled registered under the School Care program </h4>
                    </div>
                    <div class="clearfix"></div>
                    <p class="more">
                        <a href="<?= site_url('website/enroll'); ?>" class="hvr-shadow-radial">Enroll</a>
                    </p>
                </div>
                <div class="banner-bottom-grid-right2" style="background-color: #0033ff">
                    <div class="col-md-6 banner-bottom-grid-right2-grid1">
                        <div class="col-xs-12 text-center" style="margin-bottom:5px;"> <h3> Step 2 </h3> </div>
                        <span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
                        <p>Fund your Child's account with our easy to use banking platform.</p>
                        <p class="more">
                            <a href="<?= site_url('website/enroll'); ?>" class="hvr-shadow-radial" style="color:#fff;">Fund</a>
                        </p>
                    </div>
                    <div class="col-md-6 banner-bottom-grid-right2-grid2" style="background-color: #339900; color:#ffffff">
                        <div class="col-xs-12 text-center" style="margin-bottom:5px;"> <h3> Step 3 </h3> </div>
                        <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                        <p> Send us more Errand and Relax <br/><br/></p>
                        <p class="more">
                            <a href="<?= site_url(''); ?>" class="hvr-shadow-radial">Shop</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //banner-bottom -->
