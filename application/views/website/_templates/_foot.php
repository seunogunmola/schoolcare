
<!-- footer -->
	<div class="footer">	
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid">
					<h3 class="title">Quick Links</h3>
					<ul>
						<li><a href="<?= site_url('website/contact'); ?>">Contact Us</a></li>
						<li><a href="<?= site_url('website/about'); ?>">About</a></li>
						<li><a href="<?= site_url('website/shop'); ?>">Shop</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid">
					
				</div>
				<div class="col-md-3 footer-grid">
					
				</div>
				<div class="col-md-3 footer-grid contact-grid">
						<h3 class="title">Contact us</h3>
						<ul>
							<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Address</li>							
							<li class="adrs"> Address Line </li>
							<li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>+234-805XXXXX</li>
							<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:example@mail.com">mail@example.com</a></li>
						</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="copy-left">
				<p>Copyright © 2016 SchoolCare. All rights reserved | Design by <a href="<?= site_url(); ?>">Schoolcare</a></p>
			</div>
			<div class="copy-left-right">
				<ul>
					<li><a href="#" class="facebook"></a></li>
					<li><a href="#" class="twitter"></a></li>
				
				</ul>	
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!--//footer-->
<!-- for bootstrap working -->
		<script src="<?php echo getResource('website/js/bootstrap.js'); ?>"> </script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>