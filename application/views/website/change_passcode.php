
<div class="container">

     <?php
	 
	  $this->load->view("website/_templates/_account_menu");
	  
	 ?>
		<div class="col-lg-8" style="padding:0px;">

			 <div class="well well-sm" style="border-radius:0px; min-height:400px;">
                <div class="row">
				
				  <div class="col-xs-12"> 
				    <h3> Settings </h3>
				  </div>
				
				 <div class="col-xs-12" style="margin-top:20px;"> 
				 
				   <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                  
				 
				   <form class="form-horizontal" action="<?= site_url('website/passcode'); ?>" method="POST" style="font-size:12px;">

                                <div class="col-lg-6">
								         <div class="col-xs-12 well text-primary" style="margin-bottom:30px;">  
										      <h4> Change Passcode </h4>
										  </div>
                                       
                                        <div class="form-group">
                                            <label class="col-xs-4 text-right">Passcode</label>
											 <div class="col-xs-8">
                                              <input class="form-control" name="passcode" type="text" required="" maxlength="4 placeholder="Passcode" value = "<?php echo set_value('passcode',$student->passcode) ?>">
                                             </div>
									   </div>
									   
                                        <div class="form-group">
                                          <label class="col-xs-4 text-right">Confirm Passcode</label>
                                             <div class="col-xs-8">
											 <input class="form-control" name="confirm_passcode" type = "text" maxlength="4" required ="" placeholder="Confirm Passcode" value = "">
                                             </div>
										</div>
                                     
                                   <div class="form-group text-right">
                                    <input type="submit" class="btn btn-primary" name="submit" value="Update" />
								  </div>
                                </div>


                      <?php echo form_close(); ?>
							 
			
				 </div>
				 
				</div>
			 </div>
 
	    </div>
	</div>
