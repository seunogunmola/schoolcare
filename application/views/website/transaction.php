
 <link href='<?php echo getResource('website/css/pagination.css'); ?>' rel='stylesheet' type='text/css'>
 
<div class="container">

    <?php
    $this->load->view("website/_templates/_account_menu");
    ?>
    <div class="col-lg-8" style="padding:0px;">

        <div class="well well-sm" style="border-radius:0px; min-height:400px;">
            <div class="row">

                <div class="col-xs-12"> 
                    <h3> Transaction History</h3>
                </div>

                <div class="col-xs-12" style="margin-top:20px;"> 

                    <form action="<?= site_url('website/transaction'); ?>" method="POST" class="form-horizontal">

                        <div class="form-group col-sm-6">
                            <label class="col-xs-4 text-right"> Type</label>
                            <div class="col-xs-8"> 
                                <select name="type" class="form-control">
                                    <option value="outgoing"> Widthrawal </option>
                                    <option value="incoming"> Deposits </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-sm-6 text-left">
                            <div class="col-xs-8"> 
                                <input type="submit" name="submit" value="Go" class="btn btn-md btn-primary" />
                            </div>
                        </div>

                    </form>

                    <table class="table table-stripped">
                        <thead>
                        <th> S/N </th>
                        <th> Amount </th>
                        
                        <?php if ($type == "incoming") { ?>
                            <th> Depositor's Name </th>
                            <?php
                        } else {
                            ?>
                            <th> Purpose </th> 
                            <?php
                        }
                        ?>
                        <th> Date  </th>
                        <th> Transaction Type </th>
                        </thead>
                        <tbody>

<?php
$sn = 0;

if (count($transactions)) {

    foreach ($transactions as $transaction):
        ?>
                                    <tr>
                                        <td style="width:7%;"> <?= ++$sn; ?></td>
                                        <td><?= $transaction->transaction_amount; ?> </td>
                                        <?php 
                                    if($type == "incoming") { ?>
                                        <td> 
                                            <?= $transaction->transaction_made_by; ?> 
                                        </td>
                                            <?php
                                         } 
                                    else {
                                            ?>
                                        <td>
                                        <?= $transaction->transaction_purpose; ?> 
                                        </td>
                                     <?php
                                        }
                                        ?>
                                        <td><?= $transaction->transaction_date; ?> </td>
                                        <td><?= isset($transaction->transaction_purpose) ? "Withdrawal" : "Deposit"; ?> </td>
                                    </tr>
        <?php
    endforeach;
}
else {
    ?>

                                <tr>
                                    <td colspan=""> <?= "No record transaction yet"; ?></td>

                                </tr>
    <?php
}
?>

                        </tbody>


                    </table>
                    
                    <div class="col-xs-12"> <?= isset($pagination)? $pagination: ""; ?> </div>
                </div>

            </div>
        </div>

    </div>
</div>
