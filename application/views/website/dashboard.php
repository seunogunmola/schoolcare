

<div class="container" style="min-height:500px;">


    <?php
    $this->load->view("website/_templates/_account_menu");
    ?>

    <div class="col-lg-8" style="padding:0px;">
        <div class="well well-sm" style="border-radius:0px; margin-bottom:5px; min-height:250px">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <img src="<?= $student->passport; ?>" alt="" class="img-rounded img-responsive" />
                </div>
                <div class="col-sm-6 col-md-8">


                    <h4>
<?= ucfirst($student->surname) . " " . ucfirst($student->othernames); ?>
                    </h4>
                    <small>

                        <cite title="San Francisco, USA"> 
<?= ucfirst($this->account_model->get_schoolname($student->school_id)->school_name); ?>

                            <i class="glyphicon glyphicon-map-marker">
                            </i></cite></small>
                    <p>
                        <i class="glyphicon glyphicon-envelope"></i> &nbsp; <?= $student->parent_email_address; ?>
                        <br />
                        <i class="glyphicon glyphicon-globe"></i> &nbsp; <?= $student->parent_phone_number; ?>
                        <br />
                        <i class="glyphicon glyphicon-gift"></i>  &nbsp; <?= $student->date_of_birth; ?> </p>
                    <!-- Split button -->
                    <div class="col-xs-12 text-center">
                        <b>Wallet Balance</b>: <?= $balance<=0? '<span class="text-danger"><b>'.$this->config->item('currency_symbol').'&nbsp;'.$balance.'</b></span>': '<span class="text-primary"><b>'.$this->config->item('currency_symbol').'&nbsp;'.$balance.'</b></span>'; ?>                           
                    </div>
                </div>
            </div>
        </div>


        <div class="well well-sm" style="border-radius:0px; min-height:300px;">
            <div class="row">

                <div class="col-xs-12"> 
                    <h3> Fund Wallet</h3>

                    <div class="col-xs-12" style="margin-top:40px;">

                        <form action="<?= site_url('website/dashboard/paymentslip'); ?>" method="POST" class="form-horizontal">

                            <div class="form-group col-sm-6">
                                <label class="col-xs-4 text-right"> Amount </label>
                                <div class="col-xs-8"> 
                                    <input type="text" name="amount" value="" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group col-sm-6 text-left">
                                <div class="col-xs-8"> 
                                    <input type="submit" name="submit" value="Go" class="btn btn-md btn-primary" />
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
