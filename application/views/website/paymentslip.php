<div class="container">
		<div class="col-lg-12" style="padding:0px;">
          
			 <div class="well well-sm" style="border-radius:0px; min-height:500px;">
                <div class="row">
				
				  <div class="col-sm-10 col-sm-offset-1"> 
				  
					   <div class="col-xs-6"> 
						<h3> Slip </h3>
					  </div>
				  
				    <div class="col-xs-6">
						<button class="btn btn-default pull-right" onClick="print_page();"> 
							 <i class="fa fa-angle-down"></i> Print
						</button>
				    </div>
				  </div>
				  
				 <div class="col-sm-10 col-sm-offset-1" style="margin-top:20px; background:#fff;" id="print_div"> 
				    
				     <table class="table table-stripped">
					   <tbody>
					     <tr>
					      <td> <b>Amount </b></td>
					      <td><?=$amount; ?> </td>
					    </tr>
						<tr>
					      <td> <b>Account Number</b></td>
					      <td><?= "447484959"; ?> </td>
					    </tr>
						<tr>
					      <td> <b>Account Name</b></td>
					      <td><?= "SchoolCare Nigeria ".$student->uniqueid; ?> </td>
					    </tr>
						<tr>
					      <td> <b>Bank</b> </td>
					      <td> Guaranty Trust Bank (GTBank)  </td>
					    </tr>
					  </tbody>
					 </table>
				 </div>
				 
				</div>
			 </div>
 
	    </div>
	</div>

	
<script>

  function print_page()
      {
		    var content = $("#print_div").html();

		    var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
			
              "<html><head><title></title><style> </style></head><body id='body'><div> <h3> Slip </h3> </div>" +content + "</body>";

            //Print Page
              window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
           
	  }
	  
</script>
