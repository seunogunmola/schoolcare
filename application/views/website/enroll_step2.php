
   <div class="container" style="min-height:500px;">
		
		<div class="col-lg-12" style="margin-top:20px; padding-bottom:100px;"> 
		      <?php echo $this->session->flashdata('msg') ? getAlertMessage($this->session->flashdata('msg'), 'info') : '' ?>
                  
                                   <form class="form-horizontal" action="<?= site_url('website/enroll'); ?>" method="POST">
                                    <div class="col-lg-6">
                                          <div class="col-xs-12 well text-primary" style="margin-bottom:30px;">
										      <h4> Basic Information </h4>
										  </div>
                                            <div class="col-md-12" style="padding:0px;">
                                                <label>Student Passport</label>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-preview thumbnail col-md-6" style="max-width:150px; border:0px;"><img width="100px" src="<?php echo !empty($student->passport) ? base_url($student->passport) : getResource('website/images/avatar.png'); ?>" /></div>
                                                    <div class="col-md-6" >
                                                        <span class="btn btn-success col-lg-8 pull-left">
                                                            <input type="file"  name="passport" style="width:180px;" />
                                                        </span>
                                                        <a href="#" class="btn btn-danger fileupload-exists col-lg-4" data-dismiss="fileupload">Remove</a>
                                                    </div>

                                                </div>
                                            </div>
                                        <div class="form-group text-left">
                                            <label class="col-xs-3 text-right">Surname</label>
											<div class="col-xs-9">
                                             <input class="form-control" name="surname" type = "text" required ="" placeholder="Surname" value = "<?php echo set_value('surname') ?>">
                                            </div>
										</div>
										
										 <input name="school_id" type="hidden" required="" value="<?= isset($school_id)? $school_id: set_value('school_id'); ?>">
										 
                                        <div class="form-group">
                                            <label class="col-xs-3 text-right">Othernames</label>
											<div class="col-xs-9">
                                             <input class="form-control" name = "othernames" type = "text" required ="" placeholder="Othernames" value = "<?php echo set_value('othernames') ?>">
                                            </div>
									   </div>
                                        <div class="form-group">
                                             <label class="col-xs-3 text-right">Gender</label>
											 <div class="col-xs-9">
                                            <?php
                                                $gender_options = array(''=>'Select Gender','Male'=>'Male','Female'=>'Female');
                                                echo form_dropdown('gender',$gender_options,  set_value('gender'),'class = "form-control" required = "required"');
                                            ?>
                                          </div>
										</div>
                                        <div class="form-group">
                                          <label class="col-xs-3 text-right">Date of Birth</label>
										  <div class="col-xs-9">
                                            <input class="form-control" name = "date_of_birth" type = "date" placeholder="Date of Birth" value = "<?php echo set_value('date_of_birth') ?>">
                                        </div>
									   </div>
                                        
                                </div>
                                <div class="col-lg-6">
								         <div class="col-xs-12 well text-primary" style="margin-bottom:30px;">  
										      <h4> Parent Information </h4>
										  </div>
                                       
                                        <div class="form-group">
                                            <label class="col-xs-4 text-right">Parent Name</label>
											 <div class="col-xs-8">
                                              <input class="form-control" name = "parent_name" type = "text" required ="" placeholder="Parent Name" value = "<?php echo set_value('parent_name') ?>">
                                             </div>
									   </div>
                                        <div class="form-group">
                                          <label class="col-xs-4 text-right">Parent Phone Number</label>
                                             <div class="col-xs-8">
											 <input class="form-control" name = "parent_phone_number" type = "text" maxlength="11" required ="" placeholder="Parent Phone Number" value = "<?php echo set_value('parent_phone_number') ?>">
                                             </div>
										</div>
                                        <div class="form-group">
                                            <label class="col-xs-4 text-right">Parent Email Address</label>
                                            <div class="col-xs-8">
  											  <input class="form-control" name = "parent_email_address" type = "email"  placeholder="Parent Email Address" value = "<?php echo set_value('parent_email_address') ?>">
                                            </div>
										</div>

                                    <div class="form-group">
                                       <label class="col-xs-4 text-right">Status</label>
									   <div class="col-xs-8">
                                        <?php
                                        $options = array('1' => 'Enabled',
                                            '2' => 'Disabled');
                                        echo form_dropdown('status', $options, set_value('status'), 'class = "form-control"')
                                        ?>
                                    </div>
								   </div>
                                   <div class="form-group text-right">
                                    <input type="submit" class="btn btn-primary" name="submit" value="Enroll" />
                                    <button type="reset" class="btn btn-danger">Reset</button>
								  </div>
                                </div>


                             <?php echo form_close(); ?>
                         
                    </div>
			</div>
	</div>
